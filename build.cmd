@echo off

dotnet restore src/Build-Dependencies/ --packages src/Build-Dependencies/packages/

powershell -NoProfile -ExecutionPolicy Bypass -Command "& '%~dp0\src\Build-Dependencies\packages\psake\4.6.0\tools\psake.ps1' build.ps1 %*; if ($psake.build_success -eq $false) { write-host "Build Failed!" -fore RED; exit 1 } else { exit 0 }"
