import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { CanActivate } from "@angular/router";
import { Router } from "@angular/router";

@Injectable()
export class AccountService implements CanActivate {
  
  private readonly userSubject = new BehaviorSubject<User>(new User());

  readonly user = this.userSubject.asObservable();

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router) {
     this.http.get<User>("/api/account/userinfo").subscribe(x => {
       this.userSubject.next(x);
     });
  }

  signin() {
    const form = document.createElement("form");
    const redirectUrl = document.createElement("input");
    form.method = "POST";
    form.action = "/api/account/signin";
    redirectUrl.value = window.location.href;
    redirectUrl.name = "redirectUrl";
    form.appendChild(redirectUrl);
    document.body.appendChild(form);
    form.submit();
  }

  signout() {
    const form = document.createElement("form");
    const redirectUrl = document.createElement("input");
    form.method = "POST";
    form.action = "/api/account/signout";
    redirectUrl.value = window.location.href;
    redirectUrl.name = "redirectUrl";
    form.appendChild(redirectUrl);
    document.body.appendChild(form);
    form.submit();
  }

  canActivate() {
    const canActivate = this.userSubject.value.isSignedIn;
    if (!canActivate) {
      this.router.navigate(["/"]);
    }
    return canActivate;
  }
}

export class User {
  isSignedIn: boolean;
  name: string;
  picture: string;
}
