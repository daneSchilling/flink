import { TestBed, async } from "@angular/core/testing";
import { PageNotFoundComponent } from "./pageNotFound.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
describe("PageNotFoundComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PageNotFoundComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it("should create the component", async(() => {
    const fixture = TestBed.createComponent(PageNotFoundComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
