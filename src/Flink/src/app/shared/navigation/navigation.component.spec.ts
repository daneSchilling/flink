import { TestBed, async } from "@angular/core/testing";
import { NavigationComponent } from "./navigation.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HttpModule } from '@angular/http';
import { Router } from "@angular/router";
describe("NavigationComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavigationComponent
      ],
      imports: [
        HttpModule
      ],
      providers: [
        {
          provide: Router,
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it("should create the component", async(() => {
    const fixture = TestBed.createComponent(NavigationComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
