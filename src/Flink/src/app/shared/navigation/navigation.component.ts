import { Component, OnDestroy } from "@angular/core";
import { Http } from "@angular/http";
import { Subscription } from "rxjs/Subscription";
import { AccountService, User } from "./../../shared/account.service";

@Component({
  selector: "navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements OnDestroy {
  
  private readonly userSub: Subscription;

  isCollapsed = true;

  user: User;
  
  constructor(
    private readonly http: Http,
    private readonly accountService: AccountService) {
    this.userSub = accountService.user.subscribe(x => {
      this.user = x;
    });
  }

  signin($event) {
    $event.preventDefault();
    this.accountService.signin();
  }

  signout($event) {
    $event.preventDefault();
    this.accountService.signout();
  }
  
  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  collapse() {
    this.isCollapsed = true;
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
