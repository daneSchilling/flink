import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class RouteDataService {
  readonly data = new BehaviorSubject<any>(null);
}
