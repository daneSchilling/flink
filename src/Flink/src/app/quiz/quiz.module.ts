import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { routing } from "./quiz.routing";
import { QuizComponent } from "./quiz/quiz.component";
import { QuizzesComponent } from "./quizzes/quizzes.component";
import { ResultComponent } from "./result/result.component";
import { QuizService } from "./quiz.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    routing
  ],
  providers: [QuizService],
  declarations: [
    QuizComponent,
    QuizzesComponent,
    ResultComponent
  ]
})
export class QuizModule { }
