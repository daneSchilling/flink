import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class QuizService {

  constructor(private readonly http: HttpClient) { }

  getAll(): Observable<QuizListModel[]> {
    return this.http.get<QuizListModel[]>("/api/quizzes");
  }
  
  get(id: string): Observable<QuizModel> {
    return this.http.get<QuizModel>(`/api/quizzes/${id}`);
  }

  grade(id: string, data: GradeRequestModel): Observable<GradeResultModel> {
    return this.http.post<GradeResultModel>(
      `/api/quizzes/${id}/grade`,
      JSON.stringify(data),
      { headers: { 'Content-Type': "application/json" } });
  }
}

export class QuizModel {
  id: string;
  name: string;
  questions: QuestionModel[];
}

export class QuestionModel {
  id: string;
  text: string;
  imageBase64: string;
  imageAltText: string;
  type: string;
  answers: string[];
  answerChoices: AnswerChoiceModel[];
  responses: string[];
}

export class AnswerChoiceModel {
  id: string;
  text: string;
  imageBase64: string;
  imageAltText: string;
}

export class QuizListModel {
  id: string;
  name: string;
  numberOfQuestions: number;
}

export class GradeRequestModel {
  responses: GradeRequestItemModel[];
}

export class GradeRequestItemModel {
  questionId: string;
  responses: string[];
}

export class GradeResultModel {
  quizId: string;
  name: string;
  gradedQuestions: GradedQuestionModel[];
  numberOfQuestions: number;
  numberOfCorrectAnswers: number;
  percentCorrect: number;
}

export class GradedQuestionModel {
  questionId: string;
  text: string;
  imageBase64: string;
  imageAltText: string;
  correctResponses: AnswerModel[];
  responses: AnswerModel[];
  isCorrect: boolean;
}

export class AnswerModel {
  text: string;
  imageBase64: string;
  imageAltText: string;
}
