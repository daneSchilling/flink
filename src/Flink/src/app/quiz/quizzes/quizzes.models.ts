export class QuizListViewModel {
  id: string;
  name: string;
  numberOfQuestions: number;
}
