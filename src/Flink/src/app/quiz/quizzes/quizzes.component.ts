import { Component, OnInit } from "@angular/core";
import { QuizService } from "./../quiz.service";
import { QuizListViewModel } from "./quizzes.models";
import { Router } from "@angular/router";

@Component({
  templateUrl: "./quizzes.component.html",
  styleUrls: ["./quizzes.component.scss"]
})
export class QuizzesComponent implements OnInit {

  quizzes = new Array<QuizListViewModel>();
  filteredQuizzes = new Array<QuizListViewModel>();
  search: string = "";

  constructor(
    private readonly quizService: QuizService,
    private readonly router: Router) { }

  ngOnInit() {
    this.getQuizzes();
  }

  getQuizzes() {
    this.quizService.getAll().subscribe(
      data => {
        this.quizzes = data;
        this.filter();
      },
      err => console.error(err)
    );
  }

  routerLink(route: string) {
    this.router.navigate([route]);
  }

  filter() {
    if (this.search !== "") {
      var normalizedSearch = this.search.toLowerCase();
      this.filteredQuizzes = this.quizzes.filter(x => x.name.toLowerCase().indexOf(normalizedSearch) !== -1
        || x.numberOfQuestions.toString().indexOf(normalizedSearch) !== -1);
    } else {
      this.filteredQuizzes = this.quizzes;
    }
  }
}
