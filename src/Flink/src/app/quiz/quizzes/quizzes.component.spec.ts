import { TestBed, async } from "@angular/core/testing";
import { QuizzesComponent } from "./quizzes.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { QuizService } from "./../quiz.service";
import { HttpClient, HttpHandler } from "@angular/common/http";
import { RouteDataService } from "./../../shared/routeData.service";
import { Router } from "@angular/router";
describe("QuizzesComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        QuizzesComponent
      ],
      providers: [
        QuizService,
        HttpClient,
        HttpHandler,
        RouteDataService,
        {
          provide: Router,
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it("should create the component", async(() => {
    const fixture = TestBed.createComponent(QuizzesComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
  it("should render title in a h2 tag", async(() => {
    const fixture = TestBed.createComponent(QuizzesComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("h2").textContent).toContain("Quizzes");
  }));
});
