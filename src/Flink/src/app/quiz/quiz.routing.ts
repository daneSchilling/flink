import { Routes, RouterModule } from "@angular/router";
import { QuizComponent } from "./quiz/quiz.component";
import { QuizzesComponent } from "./quizzes/quizzes.component";
import { ResultComponent } from "./result/result.component";

const routes: Routes = [
  {
    path: "quizzes",
    component: QuizzesComponent
  },
  {
    path: "quizzes/result",
    component: ResultComponent
  },
  {
    path: "quizzes/:id",
    component: QuizComponent
  }
];

export const routing = RouterModule.forChild(routes);
