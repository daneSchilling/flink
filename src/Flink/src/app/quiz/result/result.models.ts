export class GradeResultViewModel {
  quizId: string;
  name: string;
  gradedQuestions: GradedQuestionViewModel[];
  numberOfQuestions: number;
  numberOfCorrectAnswers: number;
  percentCorrect: number;
}

export class GradedQuestionViewModel {
  questionId: string;
  text: string;
  imageBase64: string;
  imageAltText: string;
  correctResponses: AnswerViewModel[];
  responses: AnswerViewModel[];
  isCorrect: boolean;
}

export class AnswerViewModel {
  text: string;
  imageBase64: string;
  imageAltText: string;
}
