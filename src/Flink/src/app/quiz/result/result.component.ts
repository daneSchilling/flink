import { Component, OnInit, OnDestroy } from "@angular/core";
import { GradeResultViewModel, GradedQuestionViewModel } from "./result.models";
import { Subscription } from "rxjs/Subscription";
import { RouteDataService } from "./../../shared/routeData.service";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  templateUrl: "./result.component.html",
  styleUrls: ["./result.component.scss"]
})
export class ResultComponent implements OnInit, OnDestroy {

  ready = false;
  result: GradeResultViewModel;
  incorrect: GradedQuestionViewModel[];
  correct: GradedQuestionViewModel[];
  readonly message = new Message();

  private resultSub: Subscription;

  constructor(
    private readonly routeDataService: RouteDataService,
    private readonly domSanitizer: DomSanitizer) {
    this.message = new Message();
    this.message.progressBar = new ProgressBar(
      new RgbColor(204, 0, 0),
      new RgbColor(71, 209, 71));
  }

  ngOnInit() {
    this.resultSub = this.routeDataService.data.subscribe(x => {
      this.result = x;
      if (this.result) {
        this.incorrect = x.gradedQuestions.filter(y => !y.isCorrect);
        this.correct = x.gradedQuestions.filter(y => y.isCorrect);
        this.composeMessage();
      } else {
        console.log(`Invalid data provided by routeDataService. Expected '${GradeResultViewModel.name}'.`);
      }
    });
  }

  ngOnDestroy(): void {
    this.resultSub.unsubscribe();
  }

  sanitizedImgSrc(img: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(img);
  }

  sanitizedImgDataSrc(img: string) {
    return this.sanitizedImgSrc(`data:image/png;base64, ${img}`);
  }

  private composeMessage() {
    if (this.result.percentCorrect < 70) {
      this.message.iconPath = "/assets/images/F.png";
      this.message.text = `Oops! Study up for next time.`;
    } else if (this.result.percentCorrect < 80) {
      this.message.iconPath = "/assets/images/D.png";
      this.message.text = `Getting there! Keep learning.`;
    } else if (this.result.percentCorrect < 90) {
      this.message.iconPath = "/assets/images/C.png";
      this.message.text = `Good job! You really know your stuff.`;
    } else if (this.result.percentCorrect < 100) {
      this.message.iconPath = "/assets/images/B.png";
      this.message.text = `Awesome! You shall become a master in no time.`;
    } else {
      this.message.iconPath = "/assets/images/A.png";
      this.message.text = `Excellent! You have become a master.`;
    }
    
    this.message.progressBar.percent = Math.round(this.result.percentCorrect);
  }

  private interpolate(val1: number, val2: number, percent: number): number {
    return ((val1 * percent) + (val2 * (100 - percent))) / 100;
  }
}

class Message {
  text: string;
  iconPath: string;
  progressBar: ProgressBar;
}

class ProgressBar {
  percent: number;

  constructor(
    public minColor: RgbColor,
    public maxColor: RgbColor) { }

  currentColor() {
    const color = new RgbColor(0, 0, 0);
    color.r = Math.round(this.interpolate(this.minColor.r, this.maxColor.r, this.percent));
    color.g = Math.round(this.interpolate(this.minColor.g, this.maxColor.g, this.percent));
    color.b = Math.round(this.interpolate(this.minColor.b, this.maxColor.b, this.percent));
    return color;
  }

  private interpolate(val1: number, val2: number, percent: number): number {
    return ((val2 * percent) + (val1 * (100 - percent))) / 100;
  }
}

class RgbColor {
  constructor(
    public r: number,
    public g: number,
    public b: number) { }

  toString(): string {
    return `rgb(${this.r},${this.g},${this.b})`;
  }
}
