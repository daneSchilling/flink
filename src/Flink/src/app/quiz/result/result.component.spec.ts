import { TestBed, async } from "@angular/core/testing";
import { ResultComponent } from "./result.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { RouteDataService } from "./../../shared/routeData.service";
describe("ResultComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ResultComponent
      ],
      providers: [
        RouteDataService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it("should create the component", async(() => {
    const fixture = TestBed.createComponent(ResultComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
