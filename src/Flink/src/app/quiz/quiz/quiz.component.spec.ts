import { TestBed, async } from "@angular/core/testing";
import { QuizComponent } from "./quiz.component";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { QuizService } from "./../quiz.service";
import { HttpClient, HttpHandler } from "@angular/common/http";
import { RouteDataService } from "./../../shared/routeData.service";
import { Router, ActivatedRoute } from "@angular/router";
describe("QuizComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        QuizComponent
      ],
      providers: [
        QuizService,
        HttpClient,
        HttpHandler,
        RouteDataService,
        {
          provide: Router,
          useClass: class { navigate = jasmine.createSpy("navigate"); }
        },
        {
          provide: ActivatedRoute,
          useValue: { "params": [{ "id": "db102299-0062-4d2b-ae00-820dbdccc78f" }] }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));
  it("should create the component", async(() => {
    const fixture = TestBed.createComponent(QuizComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
