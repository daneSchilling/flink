export class QuizViewModel {
  id: string;
  name: string;
  questions: QuestionViewModel[];
}

export class QuestionViewModel {
  id: string;
  text: string;
  imageBase64: string;
  imageAltText: string;
  type: string;
  answers: string[];
  answerChoices: AnswerChoiceViewModel[];
  responses: string[];
}

export class AnswerChoiceViewModel {
  id: string;
  text: string;
  imageBase64: string;
  imageAltText: string;
}

export class GradeRequestViewModel {
  responses: GradeRequestItemViewModel[];
}

export class GradeRequestItemViewModel {
  questionId: string;
  responses: string[];
}
