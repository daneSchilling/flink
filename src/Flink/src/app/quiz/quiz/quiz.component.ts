import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { QuizService } from "./../quiz.service";
import { RouteDataService } from "./../../shared/routeData.service";
import {
  QuizViewModel,
  QuestionViewModel,
  AnswerChoiceViewModel,
  GradeRequestViewModel,
  GradeRequestItemViewModel
} from "./quiz.models";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  templateUrl: "./quiz.component.html",
  styleUrls: ["./quiz.component.scss"]
})
export class QuizComponent implements OnInit {

  quiz: QuizViewModel;

  readonly questionTypes;

  constructor(
    private readonly quizService: QuizService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly routeDataService: RouteDataService,
    private readonly domSanitizer: DomSanitizer) {
    this.questionTypes = {
      checkbox: "Checkbox",
      choice: "Choice",
      number: "Number",
      text: "Text"
    };
  }

  ngOnInit() {
    this.getQuiz(this.route.snapshot.params.id);
  }

  getQuiz(id: string) {
    this.quizService.get(id).subscribe(
      data => {
        this.quiz = data;
        this.quiz.questions.forEach(x => x.responses = new Array<string>());
      },
      err => console.error(err)
    );
  }

  onSelectionChange(question: QuestionViewModel, answer: AnswerChoiceViewModel) {
    switch (question.type) {
      case this.questionTypes.checkbox:
        const index = question.responses.indexOf(answer.id);
        if (index === -1) {
          question.responses.push(answer.id);
        } else {
          question.responses.slice(index, 1);
        }
        console.log(question.responses.length);
        break;
      case this.questionTypes.choice:
        question.responses = new Array(answer.id);
        break;
      default:
        console.log(`Invalid question type ${question.type} selected.`);
    }
  }

  onChange(question: QuestionViewModel, event: any) {
    switch (question.type) {
      case this.questionTypes.number:
      case this.questionTypes.text:
      question.responses = new Array(event.target.value);
      break;
    default:
      console.log(`Invalid question type ${question.type} selected.`);
    }
  }

  onSubmitClicked(): void {
    const postData = new GradeRequestViewModel();
    postData.responses = this.quiz.questions.map(x => {
      var item = new GradeRequestItemViewModel();
      item.questionId = x.id;
      item.responses = x.responses;
      return item;
    });
    
    this.quizService.grade(this.quiz.id, postData).subscribe(
      data => {
        this.routeDataService.data.next(data);
        this.router.navigate(["/quizzes/result"]);
      },
      err => console.error(err)
    );
  }

  sanitizedImgDataSrc(img: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(`data:image/png;base64, ${img}`);
  }
}
