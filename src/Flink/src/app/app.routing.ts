import { Routes, RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./shared/pageNotFound/pageNotFound.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/quizzes",
    pathMatch: "full"
  },
  {
    path: "**",
    component: PageNotFoundComponent
  }
];

export const routing = RouterModule.forRoot(routes);
