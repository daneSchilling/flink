import { BrowserModule } from "@angular/platform-browser";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { AppComponent } from "./app.component";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { PageNotFoundComponent } from "./shared/pageNotFound/pageNotFound.component";
import { NavigationComponent } from "./shared/navigation/navigation.component";
import { GeneralModule } from "./general/general.module";
import { QuizModule } from "./quiz/quiz.module";
import { RouteDataService } from "./shared/routeData.service";
import { AccountService } from "./shared/account.service";
import { routing } from "./app.routing";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    QuizModule,
    GeneralModule,
    routing,
    NgbModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    RouteDataService,
    AccountService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
