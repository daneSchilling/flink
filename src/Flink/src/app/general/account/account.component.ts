import { Component, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs/Subscription";
import { AccountService, User } from "./../../shared/account.service";

@Component({
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.scss"]
})
export class AccountComponent implements OnDestroy {
  private readonly userSub: Subscription;

  isCollapsed = true;

  user: User;

  constructor(
    private readonly accountService: AccountService) {
    this.userSub = accountService.user.subscribe(x => {
      this.user = x;
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
