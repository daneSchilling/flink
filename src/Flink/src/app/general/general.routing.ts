import { Routes, RouterModule } from "@angular/router";
import { AboutComponent } from "./about/about.component";
import { AccountComponent } from "./account/account.component";
import { AccountService } from "./../shared/account.service"

const routes: Routes = [
  {
    path: "about",
    component: AboutComponent
  },
  {
    path: "account",
    component: AccountComponent,
    canActivate: [
      AccountService
    ]
  }
];

export const routing = RouterModule.forChild(routes);
