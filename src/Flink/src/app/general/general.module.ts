import { CommonModule } from "@angular/common";
import { AboutComponent } from "./about/about.component";
import { AccountComponent } from "./account/account.component";
import { NgModule } from "@angular/core";
import { routing } from "./general.routing";
import { AccountService } from "./../shared/account.service"

@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [
    AboutComponent,
    AccountComponent
  ],
  providers: [
    AccountService
  ]
})
export class GeneralModule { }
