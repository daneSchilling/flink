webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<navigation></navigation>\r\n<div class=\"container-fluid main-content p-4\">\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: "app-root",
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_pageNotFound_pageNotFound_component__ = __webpack_require__("./src/app/shared/pageNotFound/pageNotFound.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_navigation_navigation_component__ = __webpack_require__("./src/app/shared/navigation/navigation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__general_general_module__ = __webpack_require__("./src/app/general/general.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__quiz_quiz_module__ = __webpack_require__("./src/app/quiz/quiz.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_routeData_service__ = __webpack_require__("./src/app/shared/routeData.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_account_service__ = __webpack_require__("./src/app/shared/account.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_routing__ = __webpack_require__("./src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__shared_pageNotFound_pageNotFound_component__["a" /* PageNotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_7__shared_navigation_navigation_component__["a" /* NavigationComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_9__quiz_quiz_module__["a" /* QuizModule */],
                __WEBPACK_IMPORTED_MODULE_8__general_general_module__["a" /* GeneralModule */],
                __WEBPACK_IMPORTED_MODULE_12__app_routing__["a" /* routing */],
                __WEBPACK_IMPORTED_MODULE_13__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NO_ERRORS_SCHEMA */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__shared_routeData_service__["a" /* RouteDataService */],
                __WEBPACK_IMPORTED_MODULE_11__shared_account_service__["a" /* AccountService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_pageNotFound_pageNotFound_component__ = __webpack_require__("./src/app/shared/pageNotFound/pageNotFound.component.ts");


var routes = [
    {
        path: "",
        redirectTo: "/quizzes",
        pathMatch: "full"
    },
    {
        path: "**",
        component: __WEBPACK_IMPORTED_MODULE_1__shared_pageNotFound_pageNotFound_component__["a" /* PageNotFoundComponent */]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forRoot(routes);


/***/ }),

/***/ "./src/app/general/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>About</h2>\r\n<p>Take a quiz and see your score!</p>\r\n"

/***/ }),

/***/ "./src/app/general/about/about.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/general/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/general/about/about.component.html"),
            styles: [__webpack_require__("./src/app/general/about/about.component.scss")]
        })
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/general/account/account.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Account</h2>\r\n<p class=\"text-danger\">This route is protected</p>\r\n<p>Your name is {{user.name}}</p>\r\n"

/***/ }),

/***/ "./src/app/general/account/account.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/general/account/account.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_account_service__ = __webpack_require__("./src/app/shared/account.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccountComponent = /** @class */ (function () {
    function AccountComponent(accountService) {
        var _this = this;
        this.accountService = accountService;
        this.isCollapsed = true;
        this.userSub = accountService.user.subscribe(function (x) {
            _this.user = x;
        });
    }
    AccountComponent.prototype.ngOnDestroy = function () {
        this.userSub.unsubscribe();
    };
    AccountComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/general/account/account.component.html"),
            styles: [__webpack_require__("./src/app/general/account/account.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_account_service__["a" /* AccountService */]])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "./src/app/general/general.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about_component__ = __webpack_require__("./src/app/general/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__account_account_component__ = __webpack_require__("./src/app/general/account/account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__general_routing__ = __webpack_require__("./src/app/general/general.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_account_service__ = __webpack_require__("./src/app/shared/account.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var GeneralModule = /** @class */ (function () {
    function GeneralModule() {
    }
    GeneralModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__general_routing__["a" /* routing */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__about_about_component__["a" /* AboutComponent */],
                __WEBPACK_IMPORTED_MODULE_2__account_account_component__["a" /* AccountComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__shared_account_service__["a" /* AccountService */]
            ]
        })
    ], GeneralModule);
    return GeneralModule;
}());



/***/ }),

/***/ "./src/app/general/general.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about_component__ = __webpack_require__("./src/app/general/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__account_account_component__ = __webpack_require__("./src/app/general/account/account.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_account_service__ = __webpack_require__("./src/app/shared/account.service.ts");




var routes = [
    {
        path: "about",
        component: __WEBPACK_IMPORTED_MODULE_1__about_about_component__["a" /* AboutComponent */]
    },
    {
        path: "account",
        component: __WEBPACK_IMPORTED_MODULE_2__account_account_component__["a" /* AccountComponent */],
        canActivate: [
            __WEBPACK_IMPORTED_MODULE_3__shared_account_service__["a" /* AccountService */]
        ]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forChild(routes);


/***/ }),

/***/ "./src/app/quiz/quiz.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__quiz_routing__ = __webpack_require__("./src/app/quiz/quiz.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__quiz_quiz_component__ = __webpack_require__("./src/app/quiz/quiz/quiz.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__quizzes_quizzes_component__ = __webpack_require__("./src/app/quiz/quizzes/quizzes.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__result_result_component__ = __webpack_require__("./src/app/quiz/result/result.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__quiz_service__ = __webpack_require__("./src/app/quiz/quiz.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var QuizModule = /** @class */ (function () {
    function QuizModule() {
    }
    QuizModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__quiz_routing__["a" /* routing */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_7__quiz_service__["a" /* QuizService */]],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__quiz_quiz_component__["a" /* QuizComponent */],
                __WEBPACK_IMPORTED_MODULE_5__quizzes_quizzes_component__["a" /* QuizzesComponent */],
                __WEBPACK_IMPORTED_MODULE_6__result_result_component__["a" /* ResultComponent */]
            ]
        })
    ], QuizModule);
    return QuizModule;
}());



/***/ }),

/***/ "./src/app/quiz/quiz.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__quiz_quiz_component__ = __webpack_require__("./src/app/quiz/quiz/quiz.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__quizzes_quizzes_component__ = __webpack_require__("./src/app/quiz/quizzes/quizzes.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__result_result_component__ = __webpack_require__("./src/app/quiz/result/result.component.ts");




var routes = [
    {
        path: "quizzes",
        component: __WEBPACK_IMPORTED_MODULE_2__quizzes_quizzes_component__["a" /* QuizzesComponent */]
    },
    {
        path: "quizzes/result",
        component: __WEBPACK_IMPORTED_MODULE_3__result_result_component__["a" /* ResultComponent */]
    },
    {
        path: "quizzes/:id",
        component: __WEBPACK_IMPORTED_MODULE_1__quiz_quiz_component__["a" /* QuizComponent */]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["d" /* RouterModule */].forChild(routes);


/***/ }),

/***/ "./src/app/quiz/quiz.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizService; });
/* unused harmony export QuizModel */
/* unused harmony export QuestionModel */
/* unused harmony export AnswerChoiceModel */
/* unused harmony export QuizListModel */
/* unused harmony export GradeRequestModel */
/* unused harmony export GradeRequestItemModel */
/* unused harmony export GradeResultModel */
/* unused harmony export GradedQuestionModel */
/* unused harmony export AnswerModel */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var QuizService = /** @class */ (function () {
    function QuizService(http) {
        this.http = http;
    }
    QuizService.prototype.getAll = function () {
        return this.http.get("/api/quizzes");
    };
    QuizService.prototype.get = function (id) {
        return this.http.get("/api/quizzes/" + id);
    };
    QuizService.prototype.grade = function (id, data) {
        return this.http.post("/api/quizzes/" + id + "/grade", JSON.stringify(data), { headers: { 'Content-Type': "application/json" } });
    };
    QuizService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], QuizService);
    return QuizService;
}());

var QuizModel = /** @class */ (function () {
    function QuizModel() {
    }
    return QuizModel;
}());

var QuestionModel = /** @class */ (function () {
    function QuestionModel() {
    }
    return QuestionModel;
}());

var AnswerChoiceModel = /** @class */ (function () {
    function AnswerChoiceModel() {
    }
    return AnswerChoiceModel;
}());

var QuizListModel = /** @class */ (function () {
    function QuizListModel() {
    }
    return QuizListModel;
}());

var GradeRequestModel = /** @class */ (function () {
    function GradeRequestModel() {
    }
    return GradeRequestModel;
}());

var GradeRequestItemModel = /** @class */ (function () {
    function GradeRequestItemModel() {
    }
    return GradeRequestItemModel;
}());

var GradeResultModel = /** @class */ (function () {
    function GradeResultModel() {
    }
    return GradeResultModel;
}());

var GradedQuestionModel = /** @class */ (function () {
    function GradedQuestionModel() {
    }
    return GradedQuestionModel;
}());

var AnswerModel = /** @class */ (function () {
    function AnswerModel() {
    }
    return AnswerModel;
}());



/***/ }),

/***/ "./src/app/quiz/quiz/quiz.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"quiz != null\">\r\n  <h2 [innerHtml]=\"quiz.name\"></h2>\r\n\r\n  <div *ngIf=\"quiz.questions.length != 0\">\r\n    <ol>\r\n      <li *ngFor=\"let question of quiz.questions; let lst = last;\">\r\n        <label\r\n          id=\"{{question.id}}_text\"\r\n          for=\"{{question.id}}_input\"\r\n          [innerHtml]=\"question.text\">\r\n        </label>\r\n        <div *ngIf=\"question.imageBase64\">\r\n          <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(question.imageBase64)\" [alt]=\"question.imageAltText\">\r\n        </div>\r\n        <div [ngSwitch]=\"question.type\">\r\n          <div *ngSwitchCase=\"questionTypes.checkbox\">\r\n            <div class=\"custom-control custom-checkbox\"\r\n                 *ngFor=\"let answerChoice of question.answerChoices\">\r\n              <input type=\"checkbox\"\r\n                     class=\"custom-control-input\"\r\n                     id=\"{{question.id}}_{{answerChoice.id}}_input\"\r\n                     name=\"{{question.id}}\"\r\n                     [value]=\"answerChoice.text\"\r\n                     (change)=\"onSelectionChange(question, answerChoice)\" />\r\n              <label class=\"custom-control-label\"\r\n                     for=\"{{question.id}}_{{answerChoice.id}}_input\"\r\n                     [attr.aria-describedby]=\"question.id + '_' + answerChoice.id + '_input'\">\r\n                {{answerChoice.text}}\r\n                <span *ngIf=\"answerChoice.imageBase64\">\r\n                  <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(answerChoice.imageBase64)\" [alt]=\"answerChoice.imageAltText\">\r\n                </span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <div *ngSwitchCase=\"questionTypes.choice\">\r\n            <div class=\"custom-control custom-radio\"\r\n                 *ngFor=\"let answerChoice of question.answerChoices\">\r\n              <input type=\"radio\"\r\n                     class=\"custom-control-input\"\r\n                     id=\"{{question.id}}_{{answerChoice.id}}_input\"\r\n                     name=\"{{question.id}}\"\r\n                     [value]=\"answerChoice.text\"\r\n                     (change)=\"onSelectionChange(question, answerChoice)\" />\r\n              <label class=\"custom-control-label\"\r\n                     for=\"{{question.id}}_{{answerChoice.id}}_input\"\r\n                     [attr.aria-describedby]=\"question.id + '_' + answerChoice.id + '_input'\">\r\n                  {{answerChoice.text}}\r\n                  <span *ngIf=\"answerChoice.imageBase64\">\r\n                    <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(answerChoice.imageBase64)\" [alt]=\"answerChoice.imageAltText\">\r\n                  </span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <div *ngSwitchCase=\"questionTypes.number\">\r\n            <input type=\"number\"\r\n                   class=\"form-control\"\r\n                   id=\"{{question.id}}_input\"\r\n                   name=\"{{question.id}}_input\"\r\n                   [attr.aria-describedby]=\"question.id + '_text'\"\r\n                   (change)=\"onChange(question, $event)\" />\r\n          </div>\r\n          <div *ngSwitchCase=\"questionTypes.text\">\r\n            <input type=\"text\"\r\n                   class=\"form-control\"\r\n                   id=\"{{question.id}}_input\"\r\n                   name=\"{{question.id}}_input\"\r\n                   [attr.aria-describedby]=\"question.id + '_text'\"\r\n                   (change)=\"onChange(question, $event)\" />\r\n          </div>\r\n          <div *ngSwitchDefault>\r\n            <p class=\"text-danger\">No layout defined for question of type {{question.type}}.</p>\r\n          </div>\r\n        </div>\r\n        <hr *ngIf=\"!lst\" />\r\n      </li>\r\n    </ol>\r\n    <br />\r\n    <button type=\"submit\" class=\"btn btn-success\" (click)=\"onSubmitClicked()\">Submit</button>\r\n    <button type=\"button\" class=\"btn btn-default\" routerLink=\"/quizzes\">Back To List</button>\r\n  </div>\r\n  <div *ngIf=\"quiz.questions.length == 0\">\r\n    <p class=\"text-muted\">There are no questions yet.</p>\r\n    <button type=\"button\" class=\"btn btn-default\" routerLink=\"/quizzes\">Back To List</button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/quiz/quiz/quiz.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/quiz/quiz/quiz.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__quiz_service__ = __webpack_require__("./src/app/quiz/quiz.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_routeData_service__ = __webpack_require__("./src/app/shared/routeData.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__quiz_models__ = __webpack_require__("./src/app/quiz/quiz/quiz.models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var QuizComponent = /** @class */ (function () {
    function QuizComponent(quizService, router, route, routeDataService, domSanitizer) {
        this.quizService = quizService;
        this.router = router;
        this.route = route;
        this.routeDataService = routeDataService;
        this.domSanitizer = domSanitizer;
        this.questionTypes = {
            checkbox: "Checkbox",
            choice: "Choice",
            number: "Number",
            text: "Text"
        };
    }
    QuizComponent.prototype.ngOnInit = function () {
        this.getQuiz(this.route.snapshot.params.id);
    };
    QuizComponent.prototype.getQuiz = function (id) {
        var _this = this;
        this.quizService.get(id).subscribe(function (data) {
            _this.quiz = data;
            _this.quiz.questions.forEach(function (x) { return x.responses = new Array(); });
        }, function (err) { return console.error(err); });
    };
    QuizComponent.prototype.onSelectionChange = function (question, answer) {
        switch (question.type) {
            case this.questionTypes.checkbox:
                var index = question.responses.indexOf(answer.id);
                if (index === -1) {
                    question.responses.push(answer.id);
                }
                else {
                    question.responses.slice(index, 1);
                }
                console.log(question.responses.length);
                break;
            case this.questionTypes.choice:
                question.responses = new Array(answer.id);
                break;
            default:
                console.log("Invalid question type " + question.type + " selected.");
        }
    };
    QuizComponent.prototype.onChange = function (question, event) {
        switch (question.type) {
            case this.questionTypes.number:
            case this.questionTypes.text:
                question.responses = new Array(event.target.value);
                break;
            default:
                console.log("Invalid question type " + question.type + " selected.");
        }
    };
    QuizComponent.prototype.onSubmitClicked = function () {
        var _this = this;
        var postData = new __WEBPACK_IMPORTED_MODULE_4__quiz_models__["b" /* GradeRequestViewModel */]();
        postData.responses = this.quiz.questions.map(function (x) {
            var item = new __WEBPACK_IMPORTED_MODULE_4__quiz_models__["a" /* GradeRequestItemViewModel */]();
            item.questionId = x.id;
            item.responses = x.responses;
            return item;
        });
        this.quizService.grade(this.quiz.id, postData).subscribe(function (data) {
            _this.routeDataService.data.next(data);
            _this.router.navigate(["/quizzes/result"]);
        }, function (err) { return console.error(err); });
    };
    QuizComponent.prototype.sanitizedImgDataSrc = function (img) {
        return this.domSanitizer.bypassSecurityTrustUrl("data:image/png;base64, " + img);
    };
    QuizComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/quiz/quiz/quiz.component.html"),
            styles: [__webpack_require__("./src/app/quiz/quiz/quiz.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__quiz_service__["a" /* QuizService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__shared_routeData_service__["a" /* RouteDataService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["b" /* DomSanitizer */]])
    ], QuizComponent);
    return QuizComponent;
}());



/***/ }),

/***/ "./src/app/quiz/quiz/quiz.models.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export QuizViewModel */
/* unused harmony export QuestionViewModel */
/* unused harmony export AnswerChoiceViewModel */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return GradeRequestViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GradeRequestItemViewModel; });
var QuizViewModel = /** @class */ (function () {
    function QuizViewModel() {
    }
    return QuizViewModel;
}());

var QuestionViewModel = /** @class */ (function () {
    function QuestionViewModel() {
    }
    return QuestionViewModel;
}());

var AnswerChoiceViewModel = /** @class */ (function () {
    function AnswerChoiceViewModel() {
    }
    return AnswerChoiceViewModel;
}());

var GradeRequestViewModel = /** @class */ (function () {
    function GradeRequestViewModel() {
    }
    return GradeRequestViewModel;
}());

var GradeRequestItemViewModel = /** @class */ (function () {
    function GradeRequestItemViewModel() {
    }
    return GradeRequestItemViewModel;
}());



/***/ }),

/***/ "./src/app/quiz/quizzes/quizzes.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Quizzes</h2>\r\n\r\n<div class=\"input-group mr-4 mb-2 d-inline-flex\" style=\"max-width:300px;\">\r\n  <input class=\"form-control\" type=\"search\" name=\"search\" placeholder=\"Search\" [(ngModel)]=\"search\" (keyup)=\"filter()\">\r\n  <div class=\"input-group-append\">\r\n    <div class=\"input-group-text\"><i class=\"fa fa-search\"></i></div>\r\n  </div>\r\n</div>\r\n<div *ngIf=\"filteredQuizzes.length === 0\" class=\"d-inline-flex\">\r\n  <h4>No quizzes match this search criteria.</h4>\r\n</div>\r\n<table *ngIf=\"filteredQuizzes.length > 0\" class=\"mt-3 table table-hover\">\r\n  <thead>\r\n  <tr>\r\n    <th>Name</th>\r\n    <th>Questions</th>\r\n  </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr *ngFor=\"let quiz of filteredQuizzes\" routerLink=\"/quizzes/{{quiz.id}}\" (keydown.enter)=\"routerLink('/quizzes/' + quiz.id)\">\r\n      <td [innerHtml]=\"quiz.name\"></td>\r\n      <td>\r\n        {{quiz.numberOfQuestions}}\r\n      </td>\r\n    </tr>\r\n  </tbody>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/quiz/quizzes/quizzes.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/quiz/quizzes/quizzes.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuizzesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__quiz_service__ = __webpack_require__("./src/app/quiz/quiz.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var QuizzesComponent = /** @class */ (function () {
    function QuizzesComponent(quizService, router) {
        this.quizService = quizService;
        this.router = router;
        this.quizzes = new Array();
        this.filteredQuizzes = new Array();
        this.search = "";
    }
    QuizzesComponent.prototype.ngOnInit = function () {
        this.getQuizzes();
    };
    QuizzesComponent.prototype.getQuizzes = function () {
        var _this = this;
        this.quizService.getAll().subscribe(function (data) {
            _this.quizzes = data;
            _this.filter();
        }, function (err) { return console.error(err); });
    };
    QuizzesComponent.prototype.routerLink = function (route) {
        this.router.navigate([route]);
    };
    QuizzesComponent.prototype.filter = function () {
        if (this.search !== "") {
            var normalizedSearch = this.search.toLowerCase();
            this.filteredQuizzes = this.quizzes.filter(function (x) { return x.name.toLowerCase().indexOf(normalizedSearch) !== -1
                || x.numberOfQuestions.toString().indexOf(normalizedSearch) !== -1; });
        }
        else {
            this.filteredQuizzes = this.quizzes;
        }
    };
    QuizzesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/quiz/quizzes/quizzes.component.html"),
            styles: [__webpack_require__("./src/app/quiz/quizzes/quizzes.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__quiz_service__["a" /* QuizService */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]])
    ], QuizzesComponent);
    return QuizzesComponent;
}());



/***/ }),

/***/ "./src/app/quiz/result/result.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"result !== null && message.text !== null\">\r\n  <h2 [innerHtml]=\"result.name\"></h2>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-3 text-center\">\r\n      <img src=\"{{message.iconPath}}\" alt=\"letter grade\"/>\r\n    </div>\r\n    <div class=\"col-md-9\">\r\n      <h2>{{message.text}}</h2>\r\n    </div>\r\n  </div>\r\n  <br/>\r\n  <div class=\"progress\" style=\"height: 40px; border-radius: 10px;\">\r\n    <div class=\"progress-bar\"\r\n         role=\"progressbar\"\r\n         [style.width]=\"message.progressBar.percent + '%'\"\r\n         [style.background-color]=\"message.progressBar.currentColor().toString()\"\r\n         [style.color]=\"'black'\"\r\n         [attr.aria-valuenow]=\"message.progressBar.percent\"\r\n         aria-valuemin=\"0\"\r\n         aria-valuemax=\"100\">{{message.progressBar.percent}}%</div>\r\n  </div>\r\n  <br />\r\n  <div *ngIf=\"result.percentCorrect != 100\">\r\n    <h4>Here's what you got wrong.</h4>\r\n    <ul>\r\n      <li *ngFor=\"let question of incorrect\">\r\n        <div>\r\n          Question: <span [innerHtml]=\"question.text\"></span>\r\n        </div>\r\n        <div *ngIf=\"question.imageBase64\">\r\n          <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(question.imageBase64)\" [alt]=\"question.imageAltText\">\r\n        </div>\r\n        <div>\r\n          <ul class=\"fa-ul\">\r\n            <li>\r\n              <i class=\"fa-li fa fa-check-circle\"></i>\r\n              <div>\r\n                <span [innerHtml]=\"question.correctResponses.length === 1 ? 'Correct Answer: ' : 'Correct Answers: '\"></span>\r\n                <span>\r\n                  <span *ngFor=\"let correctResponse of question.correctResponses\" class=\"d-block\">\r\n                    <span *ngIf=\"correctResponse.text\" class=\"d-block\">\r\n                      \"<span [innerHtml]=\"correctResponse.text\"></span>\"\r\n                    </span>\r\n                    <span *ngIf=\"correctResponse.imageBase64\" class=\"d-block\">\r\n                      <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(correctResponse.imageBase64)\" [alt]=\"correctResponse.imageAltText\">\r\n                    </span>\r\n                  </span>\r\n                </span>\r\n              </div>\r\n            </li>\r\n            <li class=\"text-danger\">\r\n              <i class=\"fa-li fa fa-times-circle\"></i>\r\n              <div>\r\n                <span [innerHtml]=\"question.responses.length === 0 ? 'Oops, you forgot to answer!' : 'Your Answer: '\"></span>\r\n                <span>\r\n                  <span *ngFor=\"let response of question.responses\" class=\"d-block\">\r\n                    <span *ngIf=\"response.text\" class=\"d-block\">\r\n                      \"<span [innerHtml]=\"response.text\"></span>\"\r\n                    </span>\r\n                    <span *ngIf=\"response.imageBase64\" class=\"d-block\">\r\n                      <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(response.imageBase64)\" [alt]=\"response.imageAltText\">\r\n                    </span>\r\n                  </span>\r\n                </span>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <div *ngIf=\"result.percentCorrect != 0\">\r\n    <h4>Here's what you got right.</h4>\r\n    <ul>\r\n      <li *ngFor=\"let question of correct\">\r\n        <div>\r\n          Question: <span [innerHtml]=\"question.text\"></span>\r\n        </div>\r\n        <div *ngIf=\"question.imageBase64\">\r\n          <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(question.imageBase64)\" [alt]=\"question.imageAltText\">\r\n        </div>\r\n        <div>\r\n          <ul class=\"fa-ul\">\r\n            <li class=\"text-success\">\r\n              <i class=\"fa-li fa fa-check-circle\"></i>\r\n              <div>\r\n                <span [innerHtml]=\"question.responses.length === 1 ? 'Answer: ' : 'Answers: '\"></span>\r\n                <span>\r\n                  <span *ngFor=\"let response of question.responses\" class=\"d-block\">\r\n                    <span *ngIf=\"response.text\" class=\"d-block\">\r\n                      \"<span [innerHtml]=\"response.text\"></span>\"\r\n                    </span>\r\n                    <span *ngIf=\"response.imageBase64\" class=\"d-block\">\r\n                      <img class=\"mb-4 mw-100 border shadow\" [src]=\"sanitizedImgDataSrc(response.imageBase64)\" [alt]=\"response.imageAltText\">\r\n                    </span>\r\n                  </span>\r\n                </span>\r\n              </div>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <br />\r\n  <button type=\"button\" class=\"btn btn-default\" routerLink=\"/quizzes\">Back To List</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/quiz/result/result.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/quiz/result/result.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__result_models__ = __webpack_require__("./src/app/quiz/result/result.models.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_routeData_service__ = __webpack_require__("./src/app/shared/routeData.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResultComponent = /** @class */ (function () {
    function ResultComponent(routeDataService, domSanitizer) {
        this.routeDataService = routeDataService;
        this.domSanitizer = domSanitizer;
        this.ready = false;
        this.message = new Message();
        this.message = new Message();
        this.message.progressBar = new ProgressBar(new RgbColor(204, 0, 0), new RgbColor(71, 209, 71));
    }
    ResultComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.resultSub = this.routeDataService.data.subscribe(function (x) {
            _this.result = x;
            if (_this.result) {
                _this.incorrect = x.gradedQuestions.filter(function (y) { return !y.isCorrect; });
                _this.correct = x.gradedQuestions.filter(function (y) { return y.isCorrect; });
                _this.composeMessage();
            }
            else {
                console.log("Invalid data provided by routeDataService. Expected '" + __WEBPACK_IMPORTED_MODULE_1__result_models__["a" /* GradeResultViewModel */].name + "'.");
            }
        });
    };
    ResultComponent.prototype.ngOnDestroy = function () {
        this.resultSub.unsubscribe();
    };
    ResultComponent.prototype.sanitizedImgSrc = function (img) {
        return this.domSanitizer.bypassSecurityTrustUrl(img);
    };
    ResultComponent.prototype.sanitizedImgDataSrc = function (img) {
        return this.sanitizedImgSrc("data:image/png;base64, " + img);
    };
    ResultComponent.prototype.composeMessage = function () {
        if (this.result.percentCorrect < 70) {
            this.message.iconPath = "/assets/images/F.png";
            this.message.text = "Oops! Study up for next time.";
        }
        else if (this.result.percentCorrect < 80) {
            this.message.iconPath = "/assets/images/D.png";
            this.message.text = "Getting there! Keep learning.";
        }
        else if (this.result.percentCorrect < 90) {
            this.message.iconPath = "/assets/images/C.png";
            this.message.text = "Good job! You really know your stuff.";
        }
        else if (this.result.percentCorrect < 100) {
            this.message.iconPath = "/assets/images/B.png";
            this.message.text = "Awesome! You shall become a master in no time.";
        }
        else {
            this.message.iconPath = "/assets/images/A.png";
            this.message.text = "Excellent! You have become a master.";
        }
        this.message.progressBar.percent = Math.round(this.result.percentCorrect);
    };
    ResultComponent.prototype.interpolate = function (val1, val2, percent) {
        return ((val1 * percent) + (val2 * (100 - percent))) / 100;
    };
    ResultComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/quiz/result/result.component.html"),
            styles: [__webpack_require__("./src/app/quiz/result/result.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_routeData_service__["a" /* RouteDataService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["b" /* DomSanitizer */]])
    ], ResultComponent);
    return ResultComponent;
}());

var Message = /** @class */ (function () {
    function Message() {
    }
    return Message;
}());
var ProgressBar = /** @class */ (function () {
    function ProgressBar(minColor, maxColor) {
        this.minColor = minColor;
        this.maxColor = maxColor;
    }
    ProgressBar.prototype.currentColor = function () {
        var color = new RgbColor(0, 0, 0);
        color.r = Math.round(this.interpolate(this.minColor.r, this.maxColor.r, this.percent));
        color.g = Math.round(this.interpolate(this.minColor.g, this.maxColor.g, this.percent));
        color.b = Math.round(this.interpolate(this.minColor.b, this.maxColor.b, this.percent));
        return color;
    };
    ProgressBar.prototype.interpolate = function (val1, val2, percent) {
        return ((val2 * percent) + (val1 * (100 - percent))) / 100;
    };
    return ProgressBar;
}());
var RgbColor = /** @class */ (function () {
    function RgbColor(r, g, b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }
    RgbColor.prototype.toString = function () {
        return "rgb(" + this.r + "," + this.g + "," + this.b + ")";
    };
    return RgbColor;
}());


/***/ }),

/***/ "./src/app/quiz/result/result.models.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GradeResultViewModel; });
/* unused harmony export GradedQuestionViewModel */
/* unused harmony export AnswerViewModel */
var GradeResultViewModel = /** @class */ (function () {
    function GradeResultViewModel() {
    }
    return GradeResultViewModel;
}());

var GradedQuestionViewModel = /** @class */ (function () {
    function GradedQuestionViewModel() {
    }
    return GradedQuestionViewModel;
}());

var AnswerViewModel = /** @class */ (function () {
    function AnswerViewModel() {
    }
    return AnswerViewModel;
}());



/***/ }),

/***/ "./src/app/shared/account.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountService; });
/* unused harmony export User */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AccountService = /** @class */ (function () {
    function AccountService(http, router) {
        var _this = this;
        this.http = http;
        this.router = router;
        this.userSubject = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](new User());
        this.user = this.userSubject.asObservable();
        this.http.get("/api/account/userinfo").subscribe(function (x) {
            _this.userSubject.next(x);
        });
    }
    AccountService.prototype.signin = function () {
        var form = document.createElement("form");
        var redirectUrl = document.createElement("input");
        form.method = "POST";
        form.action = "/api/account/signin";
        redirectUrl.value = window.location.href;
        redirectUrl.name = "redirectUrl";
        form.appendChild(redirectUrl);
        document.body.appendChild(form);
        form.submit();
    };
    AccountService.prototype.signout = function () {
        var form = document.createElement("form");
        var redirectUrl = document.createElement("input");
        form.method = "POST";
        form.action = "/api/account/signout";
        redirectUrl.value = window.location.href;
        redirectUrl.name = "redirectUrl";
        form.appendChild(redirectUrl);
        document.body.appendChild(form);
        form.submit();
    };
    AccountService.prototype.canActivate = function () {
        var canActivate = this.userSubject.value.isSignedIn;
        if (!canActivate) {
            this.router.navigate(["/"]);
        }
        return canActivate;
    };
    AccountService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* Router */]])
    ], AccountService);
    return AccountService;
}());

var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "./src/app/shared/navigation/navigation.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\r\n  <div class=\"container-fluid main-content\">\r\n    <a class=\"navbar-brand\" routerLink=\"/\" (click)=\"collapse()\">Flink</a>\r\n    <button class=\"navbar-toggler\"\r\n            type=\"button\"\r\n            data-toggle=\"collapse\"\r\n            data-target=\"#navbar-menu\"\r\n            aria-controls=\"navbar-menu\"\r\n            aria-expanded=\"false\"\r\n            aria-label=\"Toggle navigation\"\r\n            (click)=\"toggleCollapse()\">\r\n      <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n\r\n    <div class=\"collapse navbar-collapse\" id=\"navbar-menu\" [class.collapse]=\"isCollapsed\">\r\n      <ul class=\"navbar-nav mr-auto\">\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLink=\"/quizzes\" (click)=\"collapse()\">Quizzes</a>\r\n        </li>\r\n        <li class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLink=\"/about\" (click)=\"collapse()\">About</a>\r\n        </li>\r\n      </ul>\r\n      <ul *ngIf=\"!user.isSignedIn\" class=\"navbar-nav navbar-right\">\r\n        <li *ngIf=\"!user.isSignedIn\" class=\"nav-item\">\r\n          <a href=\"\" (click)=\"signin($event)\" class=\"nav-link\">Sign in</a>\r\n        </li>\r\n      </ul>\r\n      <ul *ngIf=\"user.isSignedIn\" class=\"navbar-nav navbar-right\">\r\n        <li *ngIf=\"user.isSignedIn\" class=\"nav-item\">\r\n          <a class=\"nav-link\" routerLink=\"/account\" (click)=\"collapse()\">\r\n            {{user.name}}\r\n            <img [src]=\"user.picture\" alt=\"profile\" class=\"ml-2 border border-dark rounded-circle profile\" />\r\n          </a>\r\n        </li>\r\n        <li *ngIf=\"user.isSignedIn\" class=\"nav-item\">\r\n          <a href=\"\" (click)=\"signout($event)\" class=\"nav-link\">Sign out</a>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/shared/navigation/navigation.component.scss":
/***/ (function(module, exports) {

module.exports = ".profile {\n  height: 20px; }\n"

/***/ }),

/***/ "./src/app/shared/navigation/navigation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_account_service__ = __webpack_require__("./src/app/shared/account.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(http, accountService) {
        var _this = this;
        this.http = http;
        this.accountService = accountService;
        this.isCollapsed = true;
        this.userSub = accountService.user.subscribe(function (x) {
            _this.user = x;
        });
    }
    NavigationComponent.prototype.signin = function ($event) {
        $event.preventDefault();
        this.accountService.signin();
    };
    NavigationComponent.prototype.signout = function ($event) {
        $event.preventDefault();
        this.accountService.signout();
    };
    NavigationComponent.prototype.toggleCollapse = function () {
        this.isCollapsed = !this.isCollapsed;
    };
    NavigationComponent.prototype.collapse = function () {
        this.isCollapsed = true;
    };
    NavigationComponent.prototype.ngOnDestroy = function () {
        this.userSub.unsubscribe();
    };
    NavigationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: "navigation",
            template: __webpack_require__("./src/app/shared/navigation/navigation.component.html"),
            styles: [__webpack_require__("./src/app/shared/navigation/navigation.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2__shared_account_service__["a" /* AccountService */]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/shared/pageNotFound/pageNotFound.component.html":
/***/ (function(module, exports) {

module.exports = "<h2 class=\"text-danger\">Oops... We couldn't find the page you were looking for!</h2>\r\n"

/***/ }),

/***/ "./src/app/shared/pageNotFound/pageNotFound.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/pageNotFound/pageNotFound.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageNotFoundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () { };
    PageNotFoundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/shared/pageNotFound/pageNotFound.component.html"),
            styles: [__webpack_require__("./src/app/shared/pageNotFound/pageNotFound.component.scss")]
        })
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/shared/routeData.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RouteDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("./node_modules/rxjs/_esm5/BehaviorSubject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var RouteDataService = /** @class */ (function () {
    function RouteDataService() {
        this.data = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](null);
    }
    RouteDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])()
    ], RouteDataService);
    return RouteDataService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_15" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map