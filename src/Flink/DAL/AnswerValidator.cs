﻿using System.Linq;
using System.Reflection;
using Flink.Models;
using FluentValidation;

namespace Flink.DAL
{
    public class AnswerValidator : AbstractValidator<Answer>
    {
        private static readonly PropertyInfo[] TypeProps = typeof(Answer).GetProperties();

        public AnswerValidator(Quiz quiz, Question question)
        {
            RuleFor(answer => answer)
                .Must(answer => ContainNoDuplicates(answer, question))
                .WithMessage(answer => $"Quiz (quiz:{quiz.Id}, question:{question.Id}, answer:{answer.Id}) must not have duplicate answer choice ids within a question");

            RuleFor(answer => answer)
                .Must(NotBeMissingData)
                .WithMessage(answer =>
                    $"Answer choice (quiz:{quiz.Id}, question:{question.Id}, answer:{answer.Id}) must upload data for all fields");
            
            RuleFor(answer => answer)
                .Must(NotHaveAltTextWithNoImage)
                .WithMessage(answer => $"Answer choice (quiz:{quiz.Id}, question:{question.Id}, answer:{answer.Id}) must not contain alt text with no image")
                .WithName(nameof(Question.AnswerChoices));
        }

        private static bool NotBeMissingData(Answer answer)
            => TypeProps.All(pi => pi.GetValue(answer) != null);

        private static bool NotHaveAltTextWithNoImage(Answer answer)
            => answer.ImageBase64 != string.Empty || answer.ImageAltText == string.Empty;

        private static bool ContainNoDuplicates(Answer answer, Question question)
            => question.AnswerChoices.Count(x => x.Id == answer.Id) == 1;
    }
}
