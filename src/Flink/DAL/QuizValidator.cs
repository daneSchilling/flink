﻿using System;
using System.Linq;
using System.Reflection;
using Flink.Models;
using FluentValidation;

namespace Flink.DAL
{
    public class QuizValidator : AbstractValidator<Quiz>
    {
        private static readonly PropertyInfo[] TypeProps = typeof(Quiz).GetProperties();

        public QuizValidator()
        {
            RuleFor(quiz => quiz)
                .Must(NameRequired)
                .WithMessage(quiz => $"Quiz (quiz:{quiz.Id}) must have a name");

            RuleFor(quiz => quiz)
                .Must(NotBeMissingData)
                .WithMessage(quiz => $"Quiz (quiz:{quiz.Id}) must upload data for all fields");

            RuleFor(quiz => quiz.Questions)
                .SetCollectionValidator(quiz => new QuestionValidator(quiz));
        }

        private static bool NameRequired(Quiz quiz)
            => !String.IsNullOrWhiteSpace(quiz.Name);

        private static bool NotBeMissingData(Quiz quiz)
            => TypeProps.All(pi => pi.GetValue(quiz) != null);
    }
}
