﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Flink.Infrastructure;
using Flink.Models;
using FluentValidation;
using FluentValidation.Results;
using Newtonsoft.Json;

namespace Flink.DAL
{
    public class FlinkDataRepository
    {
        private readonly SortedSet<Quiz> _quizzes = new SortedSet<Quiz>(new FuncComparer<Quiz>((x,y) =>
        {
            var result = String.Compare(x.Name, y.Name, StringComparison.Ordinal);
            if (result == 0) result = -1;
            return result;
        }));
        
        public FlinkDataRepository(
            string dataRootDirectoryPath,
            SearchOption searchOption,
            AbstractValidator<Quiz> validator)
        {
            var quizFilePaths = Directory.EnumerateFiles(dataRootDirectoryPath, "*.quiz.json", searchOption);

            var errors = new List<(string path, ValidationFailure error)>();
            foreach (var path in quizFilePaths)
            {
                var rawJson = File.ReadAllText(path);
                var quiz = JsonConvert.DeserializeObject<Quiz>(rawJson);
                errors.AddRange(validator.Validate(quiz).Errors.Select(x => (path,x)));
                _quizzes.Add(quiz);
            }
            
            if (errors.Count() != 0)
            {
                throw new AggregateException(errors.Select(e =>
                    new InvalidDataException($"Invalid quiz data {e.path} uploaded: {e.error.ErrorMessage}")));
            }
        }

        public FlinkDataRepository(
            IEnumerable<Quiz> quizzes,
            AbstractValidator<Quiz> validator)
        {
            foreach (var quiz in quizzes)
            {
                var result = validator.Validate(quiz);
                if (!result.IsValid)
                {
                    throw new AggregateException(result.Errors.Select(e =>
                        new InvalidDataException($"Invalid quiz data: {e.ErrorMessage}")));
                }

                _quizzes.Add(quiz);
            }
        }

        public IEnumerable<Quiz> GetAll()
        {
            return _quizzes.AsEnumerable();
        }
    }
}
