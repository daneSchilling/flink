﻿using System;
using System.Linq;
using System.Reflection;
using Flink.Models;
using FluentValidation;

namespace Flink.DAL
{
    public class QuestionValidator : AbstractValidator<Question>
    {
        private static readonly PropertyInfo[] TypeProps = typeof(Question).GetProperties();

        public QuestionValidator(Quiz quiz)
        {
            RuleFor(question => question)
                .Must(question => ContainNoDuplicates(question, quiz))
                .WithMessage(question => $"Question (quiz:{quiz.Id}, question:{question.Id}) must not contain duplicate question ids");

            RuleFor(question => question)
                .Must(NotBeMissingData)
                .WithMessage(question => $"Question (quiz:{quiz.Id}, question:{question.Id}) must upload data for all fields");
            
            RuleFor(question => question)
                .Must(HaveValidType)
                .WithMessage(question => $"Question (quiz:{quiz.Id}, question:{question.Id}) must have a valid type");
            
            RuleFor(question => question)
                .Must(NotHaveAltTextWithNoImage)
                .WithMessage(question => $"Question (quiz:{quiz.Id}, question:{question.Id}) must not contain alt text with no image");

            RuleFor(question => question)
                .Must(HaveAtLeastOneAnswerChoiceIfSelectionQuestion)
                .WithMessage(question => $"Selection question (quiz:{quiz.Id}, question:{question.Id}) must have at least one answer choice")
                .WithName(nameof(Question.AnswerChoices));

            RuleFor(question => question)
                .Must(HaveNoAnswerChoicesIfNotSelectionQuestion)
                .WithMessage(question => $"Non-selection question (quiz:{quiz.Id}, question:{question.Id}) cannot contain any answer choices")
                .WithName(nameof(Question.AnswerChoices));

            RuleFor(question => question)
                .Must(ContainNoDuplicateAnswers)
                .WithMessage(question => $"Question (quiz:{quiz.Id}, question:{question.Id}) must not contain duplicate answers")
                .WithName(nameof(Question.Answers));

            RuleFor(question => question)
                .Must(HaveAtLeastOneAnswer)
                .WithMessage(question => $"Question (quiz:{quiz.Id}, question:{question.Id}) must have at least one answer")
                .WithName(nameof(Question.Answers));

            RuleFor(question => question)
                .Must(ContainAllAnswersInAnswerChoicesIfSelectionQuestion)
                .WithMessage(question => $"Selection question (quiz:{quiz.Id}, question:{question.Id}) must contain all its answers within its answer choices")
                .WithName(nameof(Question.Answers));

            RuleFor(question => question)
                .Must(ContainOnlyOneAnswerIfChoiceQuestion)
                .WithMessage(question => $"Choice question (quiz:{quiz.Id}, question:{question.Id}) must contain only one answer")
                .WithName(nameof(Question.Answers));

            RuleFor(question => question.AnswerChoices)
                .SetCollectionValidator(question => new AnswerValidator(quiz, question));
        }

        private static bool ContainNoDuplicates(Question question, Quiz quiz)
            => quiz.Questions.Count(x => x.Id == question.Id) == 1;

        private static bool NotBeMissingData(Question question)
            => TypeProps.All(pi => pi.GetValue(question) != null);

        private static bool HaveValidType(Question question)
            => Enum.IsDefined(typeof(QuestionType), question.Type);

        private static bool NotHaveAltTextWithNoImage(Question question)
            => question.ImageBase64 != string.Empty || question.ImageAltText == string.Empty;

        private static bool HaveAtLeastOneAnswerChoiceIfSelectionQuestion(Question question)
            => !IsSelectionQuestion(question) || question.AnswerChoices.Any();

        private static bool HaveNoAnswerChoicesIfNotSelectionQuestion(Question question)
            => IsSelectionQuestion(question) || !question.AnswerChoices.Any();

        private static bool ContainNoDuplicateAnswers(Question question)
            => question.Answers.Count() == question.Answers.Distinct().Count();

        private static bool HaveAtLeastOneAnswer(Question question)
            => question.Answers.Any();

        private static bool ContainAllAnswersInAnswerChoicesIfSelectionQuestion(Question question)
            => !IsSelectionQuestion(question) || !question.Answers.Except(question.AnswerChoices.Select(x => x.Id)).Any();

        private static bool ContainOnlyOneAnswerIfChoiceQuestion(Question question)
            => question.Type != QuestionType.Choice || question.Answers.Count() == 1;

        private static bool IsSelectionQuestion(Question question)
            => question.Type == QuestionType.Checkbox || question.Type == QuestionType.Choice;
    }
}
