﻿using System;
using System.Collections.Generic;

namespace Flink.Models
{
    public class Quiz
    {
        public Quiz()
        {
          Questions = new List<Question>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Question> Questions { get; set; }
    }
}
