using System;
using System.Collections.Generic;

namespace Flink.Models
{
    public class Question
    {
        public Question()
        {
            Answers = new List<string>();
            AnswerChoices = new List<Answer>();
        }

        public string Id { get; set; }
        public string Text { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageAltText { get; set; }
        public QuestionType Type { get; set; }
        public List<string> Answers { get; set; }
        public List<Answer> AnswerChoices { get; set; }
        public int Order { get; set; }
    }
}
