namespace Flink.Models
{
    public class Answer
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageAltText { get; set; }
        public int Order { get; set; }
    }
}
