﻿using System.Threading;
using System.Threading.Tasks;
using Flink.Features.SignIn;
using Flink.Features.SignOut;
using Flink.Features.UserInfo;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Controllers
{
    [Route("api/account")]

    public class AccountController : Controller
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("signin")]
        public async Task<IActionResult> SignIn(SignInRequest request, CancellationToken token)
        {
            var result = await _mediator.Send(request, token);
            return result.ChallengeResult;
        }

        [HttpPost("signout")]
        public async Task<IActionResult> SignOut(SignOutRequest request, CancellationToken token)
        {
            var result = await _mediator.Send(request, token);
            return result.RedirectResult;
        }

        [HttpGet("userinfo")]
        public async Task<JsonResult> UserInfo(CancellationToken token)
        {
            var result = await _mediator.Send(new UserInfoRequest(), token);
            return Json(result);
        }
    }
}
