﻿using System.Threading;
using System.Threading.Tasks;
using Flink.Features.GetQuiz;
using Flink.Features.GetQuizzes;
using Flink.Features.GradeQuiz;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Controllers
{
    [Route("api/quizzes")]
    public class QuizzesController : Controller
    {
        private readonly IMediator _mediator;

        public QuizzesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<JsonResult> Get(CancellationToken token)
        {
            var result = await _mediator.Send(new GetQuizzesRequest(), token);
            return Json(result);
        }
 
        [HttpGet("{id}")]
        public async Task<JsonResult> Get(GetQuizRequest request, CancellationToken token)
        {
            var result = await _mediator.Send(request, token);
            return Json(result);
        }

        [HttpPost("{id}/grade")]
        public async Task<JsonResult> Post(GradeQuizRequest request, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(request, cancellationToken);
            return Json(result);
        }
    }
}
