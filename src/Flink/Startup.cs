﻿using System.IO;
using AutoMapper;
using Flink.DAL;
using Flink.Extensions;
using Flink.Features.GetQuiz;
using Flink.Features.GetQuizzes;
using Flink.Infrastructure;
using Flink.Models;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Swashbuckle.AspNetCore.Swagger;

namespace Flink
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfileConfiguration());
            });
            Mapper.AssertConfigurationIsValid();

            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddDefaultTokenProviders();

            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = Program.AppScheme;
                    options.DefaultAuthenticateScheme = Program.AppScheme;
                    options.DefaultChallengeScheme = Program.OpenIdScheme;
                })
                .AddCookie(Program.AppScheme)
                .AddOpenIdConnect(Program.OpenIdScheme, options =>
                {
                    options.SignInScheme = Program.AppScheme;
                    options.ClientId = Configuration["Authentication:Google:ClientId"];
                    options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                    options.Authority = "https://accounts.google.com";
                    options.ResponseType = OpenIdConnectResponseType.Code;
                    options.GetClaimsFromUserInfoEndpoint = true;
                    options.SaveTokens = true;
                });

            services
                .AddMvc(options => { options.Filters.Add(typeof(ValidateModelStateAttribute)); })
                .AddFluentValidation(fv =>
                {
                    ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;
                    fv.RegisterValidatorsFromAssemblyContaining<GetQuizRequestValidator>();
                });
            services.AddMediatR(typeof(GetQuizzesRequestHandler));

            services.AddQuizGradingServices();
            services.AddFlinkQuizRepository("Data", SearchOption.AllDirectories);

            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Flink API", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Use(async (context, next) => {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Flink API V1");
            });
            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.EagerlyLoadDatabase();
        }
    }
}
