﻿namespace Flink.Extensions
{
  public static class StringExtensions
  {
    public static string HtmlEncode(this string str)
    {
      return System.Net.WebUtility.HtmlEncode(str);
    }
  }
}