﻿using Flink.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Flink.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder EagerlyLoadDatabase(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.ApplicationServices.GetRequiredService<FlinkDataRepository>();
            return applicationBuilder;
        }
    }
}
