﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Flink.Extensions
{
  public static class EnumerableExtensions
    {
        private static readonly ThreadLocal<Random> RandomNumberGenerator = new ThreadLocal<Random>(() => new Random());
        
        public static IOrderedEnumerable<T> OrderByRandom<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.OrderBy(x => RandomNumberGenerator.Value.Next());
        }

        public static IOrderedEnumerable<T> ThenByRandom<T>(this IOrderedEnumerable<T> enumerable)
        {
            return enumerable.ThenBy(x => RandomNumberGenerator.Value.Next());
        }
    }
}
