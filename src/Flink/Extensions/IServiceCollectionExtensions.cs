﻿using System.Collections.Generic;
using System.IO;
using Flink.DAL;
using Flink.Features.GradeQuiz.AnswerBuilders;
using Flink.Features.GradeQuiz.ResponseGraders;
using Flink.Models;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace Flink.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddQuizGradingServices(this IServiceCollection services) =>
            services
                .AddSingleton<IResponseGrader>(new ResponseGrader(new List<IGrader>
                {
                    new CheckboxGrader(),
                    new ChoiceGrader(),
                    new NumberGrader(),
                    new TextGradder()
                }))
                .AddSingleton<IAnswerBuilder>(new AnswerBuilder(new List<IAnswerTypeBuilder>
                {
                    new CheckboxAnswerTypeBuilder(),
                    new ChoicesAnswerTypeBuilder(),
                    new NumberAnswerTypeBuilder(),
                    new TextAnswerTypeBuilder()
                }));

        public static IServiceCollection AddFlinkQuizRepository(
            this IServiceCollection services,
            string dataRootDirectoryPath,
            SearchOption searchOption) =>
            services
                .AddSingleton<AbstractValidator<Quiz>,QuizValidator>()
                .AddSingleton(x => new FlinkDataRepository(
                    dataRootDirectoryPath,
                    searchOption,
                    x.GetRequiredService<AbstractValidator<Quiz>>()));
    }
}
