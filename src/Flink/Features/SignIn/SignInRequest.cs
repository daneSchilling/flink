using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Features.SignIn
{
    public class SignInRequest : IRequest<SignInModel>
    {
        public string RedirectUrl { get; set; }
    }

    public class SignInModel
    {
        public ChallengeResult ChallengeResult { get; set; }
    }
}
