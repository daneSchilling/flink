﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Features.SignIn
{
    public class SignInRequestHandler : IRequestHandler<SignInRequest, SignInModel>
    {
        public Task<SignInModel> Handle(SignInRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(SignInRequest(request));
        }

        private SignInModel SignInRequest(SignInRequest request)
        {
            var authProperties = new AuthenticationProperties
            {
                RedirectUri = request.RedirectUrl
            };

            return new SignInModel{ ChallengeResult = new ChallengeResult("google", authProperties) };
        }
    }
}
