﻿using System.Collections.Generic;
using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.AnswerBuilders
{
    public class TextAnswerTypeBuilder : IAnswerTypeBuilder
    {
        public QuestionType Handles { get; } = QuestionType.Text;

        public List<AnswerResponseModel> Build(Question question)
            => question.Answers.Select(answered => new AnswerResponseModel
            {
                Text = answered,
                ImageBase64 = string.Empty,
                ImageAltText = string.Empty
            }).ToList();

        public List<AnswerResponseModel> Build(Question question, ResponseModel response)
            => response.Responses.Select(responded => new AnswerResponseModel
            {
                Text = responded,
                ImageBase64 = string.Empty,
                ImageAltText = string.Empty
            }).ToList();
    }
}
