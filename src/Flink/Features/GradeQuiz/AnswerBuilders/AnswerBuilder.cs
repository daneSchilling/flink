﻿using System.Collections.Generic;
using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.AnswerBuilders
{
    public class AnswerBuilder : IAnswerBuilder
    {
        private readonly IDictionary<QuestionType, IAnswerTypeBuilder> _answerBuilders;

        public AnswerBuilder(IEnumerable<IAnswerTypeBuilder> answerBuilders)
        {
            _answerBuilders = answerBuilders.ToDictionary(x => x.Handles, x => x);
        }

        public List<AnswerResponseModel> Build(Question question)
        {
            var builder = _answerBuilders[question.Type];
            return builder.Build(question);
        }

        public List<AnswerResponseModel> Build(Question question, ResponseModel response)
        {
            var builder = _answerBuilders[question.Type];
            return builder.Build(question, response);
        }
    }
}
