﻿using System.Collections.Generic;
using Flink.Models;

namespace Flink.Features.GradeQuiz.AnswerBuilders
{
    public interface IAnswerBuilder
    {
        List<AnswerResponseModel> Build(Question question);

        List<AnswerResponseModel> Build(Question question, ResponseModel response);
    }
}
