﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Flink.Models;

namespace Flink.Features.GradeQuiz.AnswerBuilders
{
    public class ChoicesAnswerTypeBuilder : IAnswerTypeBuilder
    {
        public QuestionType Handles { get; } = QuestionType.Choice;

        public List<AnswerResponseModel> Build(Question question)
        {
            var answers = question.AnswerChoices.Where(x => question.Answers.Any(a => a == x.Id));
            return answers.Select(Mapper.Map<AnswerResponseModel>).ToList();
        }

        public List<AnswerResponseModel> Build(Question question, ResponseModel response)
        {
            var responses = question.AnswerChoices.Where(x => response.Responses.Any(a => a == x.Id));
            return responses.Select(Mapper.Map<AnswerResponseModel>).ToList();
        }
    }
}
