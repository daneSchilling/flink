using System.Linq;
using Flink.DAL;
using FluentValidation;

namespace Flink.Features.GradeQuiz
{
    public class GradeQuizRequestValidator : AbstractValidator<GradeQuizRequest>
    {
        private readonly FlinkDataRepository _flinkDataRepository;
        
        public GradeQuizRequestValidator(FlinkDataRepository flinkDataRepository)
        {
            _flinkDataRepository = flinkDataRepository;

            RuleFor(x => x)
                .Must(BeValidQuizId).WithMessage("Quiz cannot be found.")
                .Must(ResponsesMustHaveValidQuestions).WithMessage("Your responses contain an invalid question.");

            RuleForEach(x => x.Body.Responses)
                .Must(OccurOnlyOnce)
                .WithMessage("Question can only have one response.");
        }

        private bool OccurOnlyOnce(GradeQuizRequest request, ResponseModel response)
        {
            var responses = request.Body.Responses;
            return !responses.Any() || responses.Count(x => x.QuestionId == response.QuestionId) <= 1;
        }

        private bool ResponsesMustHaveValidQuestions(GradeQuizRequest request)
        {
            var quiz = _flinkDataRepository.GetAll().Single(x => x.Id == request.Id);
            var responses = request.Body.Responses;
            return !responses.Any() || responses.All(x => quiz.Questions.Any(q => q.Id == x.QuestionId));
        }

        private bool BeValidQuizId(GradeQuizRequest request)
        {
            return _flinkDataRepository.GetAll().Count(x => x.Id == request.Id) == 1;
        }
    }
}
