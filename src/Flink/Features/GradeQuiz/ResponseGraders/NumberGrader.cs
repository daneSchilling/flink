using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public class NumberGrader : IGrader
    {
        public QuestionType Handles { get; } = QuestionType.Number;

        public bool Grade(Question question, ResponseModel response)
            => response.Responses.Any() && question.Answers.Single() == response.Responses.Single();
    }
}
