﻿using System.Collections.Generic;
using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public class ResponseGrader : IResponseGrader
    {
        private readonly Dictionary<QuestionType,IGrader> _graders;

        public ResponseGrader(IEnumerable<IGrader> graders)
        {
            _graders = graders.ToDictionary(x => x.Handles, x => x);
        }

        public bool Grade(Question question, ResponseModel response)
        {
            var grader = _graders[question.Type];
            return grader.Grade(question, response);
        }
    }
}
