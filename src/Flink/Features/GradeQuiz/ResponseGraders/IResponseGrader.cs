﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public interface IResponseGrader
    {
        bool Grade(Question question, ResponseModel response);
    }
}
