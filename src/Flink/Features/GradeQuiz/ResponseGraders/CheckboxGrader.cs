using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public class CheckboxGrader : IGrader
    {
        public QuestionType Handles { get; } = QuestionType.Checkbox;

        public bool Grade(Question question, ResponseModel response)
            => response.Responses.Any()
            && question.Answers.Count() == response.Responses.Count() 
            && !question.Answers.Except(response.Responses).Any();
    }
}
