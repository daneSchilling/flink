using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public class ChoiceGrader : IGrader
    {
        public QuestionType Handles { get; } = QuestionType.Choice;

        public bool Grade(Question question, ResponseModel response)
            => response.Responses.Any() && question.Answers.Single() == response.Responses.Single();
    }
}
