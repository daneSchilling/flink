using System.Linq;
using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public class TextGradder : IGrader
    {
        public QuestionType Handles { get; } = QuestionType.Text;

        public bool Grade(Question question, ResponseModel response)
            => response.Responses.Any() && question.Answers
               .Any(a => Normalize(a) == response.Responses.Select(Normalize).Single());

        private static string Normalize(string value)
            => value.Trim().ToLowerInvariant();
    }
}
