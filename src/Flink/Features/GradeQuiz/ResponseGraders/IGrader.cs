﻿using Flink.Models;

namespace Flink.Features.GradeQuiz.ResponseGraders
{
    public interface IGrader
    {
        QuestionType Handles { get; }
        bool Grade(Question question, ResponseModel response);
    }
}
