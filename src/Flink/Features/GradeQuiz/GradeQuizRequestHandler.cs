﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Flink.DAL;
using Flink.Extensions;
using Flink.Features.GradeQuiz.AnswerBuilders;
using Flink.Features.GradeQuiz.ResponseGraders;
using Flink.Models;
using MediatR;

namespace Flink.Features.GradeQuiz
{
    public class GradeQuizRequestHandler : IRequestHandler<GradeQuizRequest, GradeResultModel>
    {
        private readonly IResponseGrader _responseGrader;
        private readonly IAnswerBuilder _answerBuilder;
        private readonly FlinkDataRepository _flinkDataRepository;

        public GradeQuizRequestHandler(
            IResponseGrader responseGrader,
            IAnswerBuilder answerBuilder,
            FlinkDataRepository flinkDataRepository)
        {
            _responseGrader = responseGrader;
            _answerBuilder = answerBuilder;
            _flinkDataRepository = flinkDataRepository;
        }

        public Task<GradeResultModel> Handle(GradeQuizRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(GradeQuiz(request));
        }

        private GradeResultModel GradeQuiz(GradeQuizRequest request)
        {
            var quiz = _flinkDataRepository.GetAll().Single(q => q.Id == request.Id);
            var responseModels = request.Body.Responses;
            var result = new GradeResultModel
            {
                QuizId = request.Id,
                Name = quiz.Name.HtmlEncode(),
                GradedQuestions = responseModels.Select(x => GradeResponse(quiz, x)).ToList(),
                NumberOfQuestions = quiz.Questions.Count,
            };
            result.NumberOfCorrectAnswers = result.GradedQuestions?.Count(q => q.IsCorrect) ?? 0;
            return result;
        }

        private GradedQuestionModel GradeResponse(Quiz quiz, ResponseModel response)
        {
            var question = quiz.Questions.Single(q => q.Id == response.QuestionId);
            return new GradedQuestionModel
            {
                QuestionId = response.QuestionId,
                Text = question.Text.HtmlEncode(),
                ImageBase64 = question.ImageBase64.HtmlEncode(),
                IsCorrect = _responseGrader.Grade(question, response),
                CorrectResponses = _answerBuilder.Build(question),
                Responses = _answerBuilder.Build(question, response),
            };
        }
    }
}
