using System;
using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Features.GradeQuiz
{
    public class GradeQuizRequest : IRequest<GradeResultModel>
    {
        public GradeQuizRequest()
        {
            Body = new Body();
        }
 
        [FromRoute(Name = "id")]
        public Guid Id { get; set; }
        [FromBody]
        public Body Body { get; set; }
    }

    public class Body
    {
        public Body()
        {
            Responses = new List<ResponseModel>();
        }

        public List<ResponseModel> Responses { get; set; }
    }

    public class ResponseModel
    {
        public ResponseModel()
        {
            Responses = new List<string>();
        }

        public string QuestionId { get; set; }
        public List<string> Responses { get; set; }
    }

    public class GradeResultModel
    {
        public GradeResultModel()
        {
            GradedQuestions = new List<GradedQuestionModel>();
        }

        public Guid QuizId { get; set; }
        public string Name { get; set; }
        public List<GradedQuestionModel> GradedQuestions { get; set; }
        public int NumberOfQuestions { get; set; }
        public int NumberOfCorrectAnswers { get; set; }

        public double PercentCorrect =>
            NumberOfQuestions > 0
                ? NumberOfCorrectAnswers / (double) NumberOfQuestions * 100D
                : 100;
    }

    public class GradedQuestionModel
    {
        public GradedQuestionModel()
        {
            CorrectResponses = new List<AnswerResponseModel>();
            Responses = new List<AnswerResponseModel>();
        }

        public string QuestionId { get; set; }
        public string Text { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageAltText { get; set; }
        public bool IsCorrect { get; set; }
        public List<AnswerResponseModel> CorrectResponses { get; set; }
        public List<AnswerResponseModel> Responses { get; set; }
    }

    public class AnswerResponseModel
    {
        public string Text { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageAltText { get; set; }
    }
}
