﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Flink.DAL;
using MediatR;

namespace Flink.Features.GetQuizzes
{
    public class GetQuizzesRequestHandler : IRequestHandler<GetQuizzesRequest, List<QuizListModel>>
    {
        private readonly FlinkDataRepository _flinkDataRepository;

        public GetQuizzesRequestHandler(FlinkDataRepository flinkDataRepository)
        {
            _flinkDataRepository = flinkDataRepository;
        }

        public Task<List<QuizListModel>> Handle(GetQuizzesRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetList(request));
        }

        public List<QuizListModel> GetList(GetQuizzesRequest request)
        {
            return _flinkDataRepository.GetAll().Select(Mapper.Map<QuizListModel>).ToList();
        }
    }
}
