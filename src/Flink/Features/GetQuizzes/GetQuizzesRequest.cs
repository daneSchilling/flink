﻿using System;
using System.Collections.Generic;
using MediatR;

namespace Flink.Features.GetQuizzes
{
    public class GetQuizzesRequest : IRequest<List<QuizListModel>>
    {
    }

    public class QuizListModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int NumberOfQuestions { get; set; }
    }
}
