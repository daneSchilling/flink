﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Features.SignOut
{
    public class SignOutRequestHandler : IRequestHandler<SignOutRequest, SignOutModel>
    {
        private readonly HttpContext _httpContext;

        public SignOutRequestHandler(IHttpContextAccessor accessor)
        {
            _httpContext = accessor.HttpContext;
        }

        public async Task<SignOutModel> Handle(SignOutRequest request, CancellationToken cancellationToken)
        {
            await _httpContext.SignOutAsync(Program.AppScheme);

            return new SignOutModel {RedirectResult = new RedirectResult(request.RedirectUrl)};
        }
    }
}
