using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Features.SignOut
{
    public class SignOutRequest : IRequest<SignOutModel>
    {
        public string RedirectUrl { get; set; }
    }

    public class SignOutModel
    {
        public RedirectResult RedirectResult { get; set; }
    }
}
