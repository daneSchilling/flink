﻿using System;
using System.Linq;
using Flink.DAL;
using FluentValidation;

namespace Flink.Features.GetQuiz
{
    public class GetQuizRequestValidator : AbstractValidator<GetQuizRequest>
    {
        private readonly FlinkDataRepository _flinkDataRepository;
        
        public GetQuizRequestValidator(FlinkDataRepository flinkDataRepository)
        {
            _flinkDataRepository = flinkDataRepository;
            RuleFor(x => x.Id).Must(BeValidId).WithMessage("Quiz cannot be found.");
        }

        private bool BeValidId(Guid id)
        {
            return _flinkDataRepository.GetAll().Count(x => x.Id == id) == 1;
        }
    }
}
