﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Flink.DAL;
using MediatR;

namespace Flink.Features.GetQuiz
{
    public class GetQuizRequestHandler : IRequestHandler<GetQuizRequest, QuizModel>
    {
        private readonly FlinkDataRepository _flinkDataRepository;

        public GetQuizRequestHandler(FlinkDataRepository flinkDataRepository)
        {
            _flinkDataRepository = flinkDataRepository;
        }

        public Task<QuizModel> Handle(GetQuizRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(GetQuizRequest(request));
        }

        private QuizModel GetQuizRequest(GetQuizRequest request)
        {
            var quiz = _flinkDataRepository.GetAll().Single(x => x.Id == request.Id);
            return Mapper.Map<QuizModel>(quiz);
        }
    }
}
