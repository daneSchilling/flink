using System;
using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Flink.Features.GetQuiz
{
    public class GetQuizRequest : IRequest<QuizModel>
    {
        [FromRoute(Name = "id")]
        public Guid Id { get; set; }
    }

    public class QuizModel
    {
        public QuizModel()
        {
            Questions = new List<QuestionModel>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<QuestionModel> Questions { get; set; }
    }

    public class QuestionModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageAltText { get; set; }
        public string Type { get; set; }
        public List<string> Answers { get; set; }
        public List<AnswerModel> AnswerChoices { get; set; }
    }

    public class AnswerModel
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageAltText { get; set; }
    }
}
