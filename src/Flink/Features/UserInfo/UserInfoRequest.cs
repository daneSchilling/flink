using MediatR;

namespace Flink.Features.UserInfo
{
    public class UserInfoRequest : IRequest<UserInfoModel>
    {
    }

    public class UserInfoModel
    {
        public bool IsSignedIn { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
    }
}
