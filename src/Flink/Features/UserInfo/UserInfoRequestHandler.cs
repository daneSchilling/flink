﻿using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Flink.Features.UserInfo
{
    public class UserInfoRequestHandler : IRequestHandler<UserInfoRequest, UserInfoModel>
    {
        private readonly ClaimsPrincipal _user;

        public UserInfoRequestHandler(IHttpContextAccessor accessor)
        {
            _user = accessor.HttpContext.User;
        }

        public Task<UserInfoModel> Handle(UserInfoRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(UserInfoRequest(request));
        }

        private UserInfoModel UserInfoRequest(UserInfoRequest request)
        {
            return new UserInfoModel
            {
                IsSignedIn = _user.Claims.Any(x => x.Type == "name"),
                Name = _user.Claims?.FirstOrDefault(x => x.Type == "name")?.Value ?? "",
                Picture = _user.Claims?.FirstOrDefault(x => x.Type == "picture")?.Value ?? ""
            };
        }
    }
}
