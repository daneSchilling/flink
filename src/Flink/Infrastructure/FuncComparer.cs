﻿using System;
using System.Collections.Generic;

namespace Flink.Infrastructure
{
    public class FuncComparer<T> : IComparer<T>
    {
        private readonly Func<T, T, int> _comparerExpression;

        public FuncComparer(Func<T, T, int> comparerExpression)
        {
            _comparerExpression = comparerExpression;
        }

        public int Compare(T x, T y)
        {
            return _comparerExpression(x, y);
        }
    }
}
