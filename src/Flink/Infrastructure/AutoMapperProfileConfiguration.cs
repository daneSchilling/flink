using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Flink.Extensions;
using Flink.Features.GetQuiz;
using Flink.Features.GetQuizzes;
using Flink.Features.GradeQuiz;
using Flink.Models;

namespace Flink.Infrastructure
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
            : this("FlinkProfile")
        {
        }

        protected AutoMapperProfileConfiguration(string profileName)
            : base(profileName)
        {
            CreateMap<Quiz, QuizListModel>()
                .ForMember(m => m.Name, c => c.ResolveUsing(q => q.Name.HtmlEncode()))
                .ForMember(m => m.NumberOfQuestions, c => c.ResolveUsing(q => q.Questions?.Count));
            CreateMap<Quiz, QuizModel>()
                .ForMember(m => m.Name, c => c.ResolveUsing(q => q.Name.HtmlEncode()))
                .ForMember(m => m.Questions,
                    c => c.ResolveUsing(q => q.Questions.OrderBy(a => a.Order).ThenByRandom().Select(Mapper.Map<QuestionModel>)));
            CreateMap<Question, QuestionModel>()
                .ForMember(m => m.Text, c => c.ResolveUsing(q => q.Text.HtmlEncode()))
                .ForMember(m => m.Type, c => c.ResolveUsing(q => q.Type.ToString()))
                .ForMember(m => m.ImageBase64, c => c.ResolveUsing(q => q.ImageBase64.HtmlEncode()))
                .ForMember(m => m.ImageAltText, c => c.ResolveUsing(q => q.ImageAltText.HtmlEncode()))
                .ForMember(m => m.Answers,
                    c => c.ResolveUsing(q => q.AnswerChoices.OrderBy(a => a.Order).ThenByRandom().Select(a => a.Text.HtmlEncode()).ToList()))
                .ForMember(m => m.AnswerChoices,
                    c => c.ResolveUsing(q => q.AnswerChoices.OrderBy(a => a.Order).ThenByRandom().Select(Mapper.Map<AnswerModel>).ToList()));
            CreateMap<Answer, AnswerModel>()
                .ForMember(x => x.Text, c => c.ResolveUsing(m => m.Text.HtmlEncode()))
                .ForMember(x => x.ImageBase64, c => c.ResolveUsing(m => m.ImageBase64.HtmlEncode()))
                .ForMember(x => x.ImageAltText, c => c.ResolveUsing(m => m.ImageAltText.HtmlEncode()));
            CreateMap<Answer, AnswerResponseModel>()
                .ForMember(x => x.Text, c => c.ResolveUsing(m => m.Text.HtmlEncode()))
                .ForMember(x => x.ImageBase64, c => c.ResolveUsing(m => m.ImageBase64.HtmlEncode()))
                .ForMember(x => x.ImageAltText, c => c.ResolveUsing(m => m.ImageAltText.HtmlEncode()));
        }
    }
}
