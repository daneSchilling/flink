﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Flink
{
    public class Program
    {
        public const string AppScheme = "app";
        public const string OpenIdScheme = "google";

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
