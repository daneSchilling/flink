﻿using Flink.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flink.Tests
{
    public class TestSetup
    {
        [TestInitialize]
        public void Setup()
        {
            AutoMapper.Mapper.Reset();
            AutoMapper.Mapper.Initialize(c => c.AddProfile(new AutoMapperProfileConfiguration()));
        }
    }
}