﻿using Flink.DAL;
using Flink.Features.GradeQuiz;
using Flink.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Flink.Tests.Features.GradeQuiz
{
    [TestClass]
    public class GradeQuizRequestValidatorTests
    {
        [TestMethod]
        public void ShouldBeValidGradeQuizResult()
        {
            (Guid q1, Guid q2) = (Guid.NewGuid(), Guid.NewGuid());
            var responses = new List<ResponseModel>
            {
                new ResponseModel
                {
                    QuestionId = q1.ToString(),
                    Responses = new List<string>{ "First response" }
                },
                new ResponseModel
                {
                    QuestionId = q2.ToString(),
                    Responses = new List<string>{ "Second response" }
                }
            };

            var quizId = Guid.NewGuid();
            var request = new GradeQuizRequest
            {
                Id = quizId,
                Body = new Body { Responses = responses }
            };

            var quiz = new Quiz
            {
                Id = quizId,
                Name = "Quiz",
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = q1.ToString(),
                        Type = QuestionType.Text,
                        Text = "q1",
                        Answers = new List<string> { "answer"},
                        AnswerChoices = new List<Answer>(),
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Question
                    {
                        Id = q2.ToString(),
                        Type = QuestionType.Text,
                        Text = "q1",
                        Answers = new List<string> { "answer"},
                        AnswerChoices = new List<Answer>(),
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                }
            };
            
            var flinkDataRespository = new FlinkDataRepository(new List<Quiz> {quiz}, new QuizValidator());
            var validator = new GradeQuizRequestValidator(flinkDataRespository);
            var result = validator.Validate(request);

            Assert.IsTrue(result.IsValid);
        }

        [TestMethod]
        public void ShouldBeInvalidGradeQuizResult()
        {
            (Guid q1, Guid q2) = (Guid.NewGuid(), Guid.NewGuid());
            var responses = new List<ResponseModel>
            {
                new ResponseModel
                {
                    QuestionId = q1.ToString(),
                    Responses = new List<string>{ "First response" }
                },
                new ResponseModel
                {
                    QuestionId = q1.ToString(),
                    Responses = new List<string>{ "Second response w/duplicated questionId" }
                }
            };

            var quiz = new Quiz
            {
                Id = Guid.NewGuid(), 
                Name = "Name",
                Questions = new List<Question>
                {
                    new Question
                    {
                        Id = q1.ToString(),
                        Type = QuestionType.Text,
                        Text = "q1",
                        Answers = { "answer" },
                        AnswerChoices = new List<Answer>(),
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Question
                    {
                        Id = q2.ToString(),
                        Type = QuestionType.Text,
                        Text = "q2",
                        Answers = { "answer" },
                        AnswerChoices = new List<Answer>(),
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                }
            };
            var flinkDataRespository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var request = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body { Responses = responses }
            };

            var validator = new GradeQuizRequestValidator(flinkDataRespository);
            var result = validator.Validate(request);

            Assert.IsFalse(result.IsValid);
            Assert.AreSame(result.Errors.First().ErrorMessage, "Question can only have one response.");
        }
    }
}
