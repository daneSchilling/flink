﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Flink.DAL;
using Flink.Features.GradeQuiz;
using Flink.Features.GradeQuiz.AnswerBuilders;
using Flink.Features.GradeQuiz.ResponseGraders;
using Flink.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flink.Tests.Features.GradeQuiz
{
    [TestClass]
    public class GradeQuizHandlerTests : TestSetup
    {
        [TestMethod]
        public void ShouldBeInvalidToHaveMissingQuiz()
        {
            var gradeQuizRequest = new GradeQuizRequest();
            var handler = new GradeQuizRequestHandler(
                new ResponseGrader(new List<IGrader>()),
                new AnswerBuilder(new List<IAnswerTypeBuilder>()),
                new FlinkDataRepository(new List<Quiz>(), new QuizValidator()));
            Assert.ThrowsException<InvalidOperationException>(() => handler.Handle(gradeQuizRequest, new CancellationToken()));
        }

        [TestMethod]
        public void ShouldGradeQuizWithMissingAnswer()
        {
            var q1 = new Question
            {
                Id = new Guid("12fc0484-826a-4eea-a873-fcc90ddc9401").ToString(),
                Text = "q1",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "d4a421ed-3e15-462b-a157-edb33f97309d"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "d4a421ed-3e15-462b-a157-edb33f97309d",
                        Text = "q1 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "327ab385-9d4f-4540-93ae-b889140e97ef",
                        Text = "q1 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "d5bd788c-2768-45de-8fd9-a47d384e50c7",
                        Text = "q1 a3",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var quiz = new Quiz
            {
                Id = new Guid("0ac95955-65aa-41a7-94a0-7eebabdb3b59"),
                Name = "Quiz Name",
                Questions = new List<Question> { q1 }
            };

            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var gradeQuizRequest = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body
                {
                    Responses = new List<ResponseModel>()
                }
            };

            var handler = GenerateGradeQuizRequestHandler(repository);
            var result = handler.Handle(gradeQuizRequest, new CancellationToken()).Result;
            Assert.AreEqual(1, result.NumberOfQuestions, "NumberOfQuestions");
            Assert.AreEqual(0, result.NumberOfCorrectAnswers, "NumberOfCorrectAnswers");
            Assert.AreEqual(0, result.GradedQuestions.Count, "GradedQuestions");
            Assert.AreEqual(0, result.PercentCorrect, "PercentCorrect");
        }

        [TestMethod]
        public void ShouldBeInvalidToResponseToQuizWithMissingQuestion()
        {
            var q1 = new Question
            {
                Id = new Guid("12fc0484-826a-4eea-a873-fcc90ddc9401").ToString(),
                Text = "q1",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "d4a421ed-3e15-462b-a157-edb33f97309d"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "d4a421ed-3e15-462b-a157-edb33f97309d",
                        Text = "q1 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "327ab385-9d4f-4540-93ae-b889140e97ef",
                        Text = "q1 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "d5bd788c-2768-45de-8fd9-a47d384e50c7",
                        Text = "q1 a3",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var quiz = new Quiz
            {
                Id = new Guid("0ac95955-65aa-41a7-94a0-7eebabdb3b59"),
                Name = "Quiz Name",
                Questions = new List<Question>()
            };

            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var gradeQuizRequest = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body
                {
                    Responses = new List<ResponseModel>
                    {
                        new ResponseModel
                        {
                            QuestionId = q1.Id,
                            Responses = new List<string> { q1.AnswerChoices.First().Id }
                        }
                    }
                }
            };

            var handler = GenerateGradeQuizRequestHandler(repository);
            Assert.ThrowsException<InvalidOperationException>(() => handler.Handle(gradeQuizRequest, new CancellationToken()));
        }

        [TestMethod]
        public void ShouldBeInvalidToProvideMultipleAnswersToChoiceQuestion()
        {
            var q1 = new Question
            {
                Id = new Guid("12fc0484-826a-4eea-a873-fcc90ddc9401").ToString(),
                Text = "q1",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "d4a421ed-3e15-462b-a157-edb33f97309d"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "d4a421ed-3e15-462b-a157-edb33f97309d",
                        Text = "q1 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "327ab385-9d4f-4540-93ae-b889140e97ef",
                        Text = "q1 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "d5bd788c-2768-45de-8fd9-a47d384e50c7",
                        Text = "q1 a3",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var quiz = new Quiz
            {
                Id = new Guid("0ac95955-65aa-41a7-94a0-7eebabdb3b59"),
                Name = "Quiz Name",
                Questions = new List<Question>()
            };

            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var gradeQuizRequest = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body
                {
                    Responses = new List<ResponseModel>
                    {
                        new ResponseModel
                        {
                            QuestionId = q1.Id,
                            Responses = q1.AnswerChoices.Select(x => x.Id).ToList()
                        }
                    }
                }
            };

            var handler = GenerateGradeQuizRequestHandler(repository);
            Assert.ThrowsException<InvalidOperationException>(() => handler.Handle(gradeQuizRequest, new CancellationToken()));
        }

        [TestMethod]
        public void ShouldBeInvalidToProvideMultipleCorrectAnswersToChoiceQuestion()
        {
            var q1 = new Question
            {
                Id = new Guid("12fc0484-826a-4eea-a873-fcc90ddc9401").ToString(),
                Text = "q1",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "d4a421ed-3e15-462b-a157-edb33f97309d", "327ab385-9d4f-4540-93ae-b889140e97ef"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "d4a421ed-3e15-462b-a157-edb33f97309d",
                        Text = "q1 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "327ab385-9d4f-4540-93ae-b889140e97ef",
                        Text = "q1 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "d5bd788c-2768-45de-8fd9-a47d384e50c7",
                        Text = "q1 a3",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var quiz = new Quiz
            {
                Id = new Guid("0ac95955-65aa-41a7-94a0-7eebabdb3b59"),
                Name = "Quiz Name",
                Questions = new List<Question>()
            };

            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var gradeQuizRequest = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body
                {
                    Responses = new List<ResponseModel>
                    {
                        new ResponseModel
                        {
                            QuestionId = q1.Id,
                            Responses = new List<string> { q1.AnswerChoices.First().Id }
                        }
                    }
                }
            };

            var handler = GenerateGradeQuizRequestHandler(repository);
            Assert.ThrowsException<InvalidOperationException>(() => handler.Handle(gradeQuizRequest, new CancellationToken()));
        }

        [TestMethod]
        public void ShouldBeAbleToSubmitQuizWithNoQuestions()
        {
            var quiz = new Quiz
            {
                Id = new Guid("0ac95955-65aa-41a7-94a0-7eebabdb3b59"),
                Name = "Quiz Name",
                Questions = new List<Question>()
            };

            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var gradeQuizRequest = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body
                {
                    Responses = new List<ResponseModel>()
                }
            };

            var handler = GenerateGradeQuizRequestHandler(repository);
            var result = handler.Handle(gradeQuizRequest, new CancellationToken()).Result;
            Assert.AreEqual(0, result.NumberOfQuestions, "NumberOfQuestions");
            Assert.AreEqual(0, result.NumberOfCorrectAnswers, "NumberOfCorrectAnswers");
            Assert.AreEqual(0, result.GradedQuestions.Count, "GradedQuestions");
            Assert.AreEqual(100, result.PercentCorrect, "PercentCorrect");
        }

        [TestMethod]
        public void ShouldGradeValidQuizCorrectly()
        {
            var q1 = new Question
            {
                Id = new Guid("12fc0484-826a-4eea-a873-fcc90ddc9401").ToString(),
                Text = "q1",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "d4a421ed-3e15-462b-a157-edb33f97309d"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "d4a421ed-3e15-462b-a157-edb33f97309d",
                        Text = "q1 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "327ab385-9d4f-4540-93ae-b889140e97ef",
                        Text = "q1 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "d5bd788c-2768-45de-8fd9-a47d384e50c7",
                        Text = "q1 a3",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var q2 = new Question
            {
                Id = new Guid("82b7374f-1a4d-4e36-9d22-3f69a01adb30").ToString(),
                Text = "q2",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "e9c67ae9-c47c-47b6-a31f-9a76cedb4361"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "e9c67ae9-c47c-47b6-a31f-9a76cedb4361",
                        Text = "q2 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "19b6ed04-4bd8-4517-bc2b-f89a35780b00",
                        Text = "q2 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var q3 = new Question
            {
                Id = new Guid("ef3efeb1-cdd4-4c51-a362-065b7abb6099").ToString(),
                Text = "q3",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "ab1029fd-5e66-4bfc-aa20-5a832e07f8fb"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "43468638-71b2-4ab9-834a-3d99f3b45763",
                        Text = "q3 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "ab1029fd-5e66-4bfc-aa20-5a832e07f8fb",
                        Text = "q3 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "9273f1d8-49de-44ec-a6a2-641f20a824b7",
                        Text = "q3 a3",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var q4 = new Question
            {
                Id = new Guid("fe958174-c496-4422-a772-a934abe65664").ToString(),
                Text = "q4",
                Type = QuestionType.Choice,
                Answers = new List<string>
                {
                    "0df8229e-57b6-48b9-805e-af2fa58e5e20"
                },
                AnswerChoices = new List<Answer>
                {
                    new Answer
                    {
                        Id = "0df8229e-57b6-48b9-805e-af2fa58e5e20",
                        Text = "q4 a1",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    },
                    new Answer
                    {
                        Id = "5a5c71c4-846b-43dd-b875-21aa0c6af486",
                        Text = "q4 a2",
                        ImageBase64 = "",
                        ImageAltText = "",
                        Order = 0
                    }
                },
                ImageBase64 = "",
                ImageAltText = "",
                Order = 0
            };

            var quiz = new Quiz
            {
                Id = Guid.NewGuid(),
                Name = "Quiz Name",
                Questions = new List<Question> { q1, q2, q3, q4 }
            };

            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var gradeQuizRequest = new GradeQuizRequest
            {
                Id = quiz.Id,
                Body = new Body
                {
                    Responses = new List<ResponseModel>
                    {
                        new ResponseModel
                        {
                            QuestionId = q1.Id,
                            Responses = new List<string> { q1.AnswerChoices.First().Id }
                        },
                        new ResponseModel
                        {
                            QuestionId = q2.Id,
                            Responses = new List<string> { q2.AnswerChoices.First().Id }
                        },
                        new ResponseModel
                        {
                            QuestionId = q3.Id,
                            Responses = new List<string> { q3.AnswerChoices.First().Id }
                        },
                        new ResponseModel
                        {
                            QuestionId = q4.Id,
                            Responses = new List<string>()
                        }
                    }
                }
            };

            var handler = GenerateGradeQuizRequestHandler(repository);
            var result = handler.Handle(gradeQuizRequest, new CancellationToken()).Result;
            Assert.AreEqual(4, result.NumberOfQuestions, "NumberOfQuestions");
            Assert.AreEqual(2, result.NumberOfCorrectAnswers, "NumberOfCorrectAnswers");
            Assert.AreEqual(4, result.GradedQuestions.Count, "GradedQuestions");
            Assert.AreEqual(50, result.PercentCorrect, "PercentCorrect");
        }

        private static GradeQuizRequestHandler GenerateGradeQuizRequestHandler(FlinkDataRepository repository) 
            => new GradeQuizRequestHandler(
                new ResponseGrader(new List<IGrader>
                {
                    new CheckboxGrader(),
                    new ChoiceGrader(),
                    new NumberGrader(),
                    new TextGradder()
                }),
                new AnswerBuilder(new List<IAnswerTypeBuilder>
                {
                    new CheckboxAnswerTypeBuilder(),
                    new ChoicesAnswerTypeBuilder(),
                    new NumberAnswerTypeBuilder(),
                    new TextAnswerTypeBuilder()
                }),
                repository);
    }
}
