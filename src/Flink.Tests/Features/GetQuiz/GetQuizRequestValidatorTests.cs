﻿using System;
using System.Collections.Generic;
using System.Linq;
using Flink.DAL;
using Flink.Features.GetQuiz;
using Flink.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flink.Tests.Features.GetQuiz
{
    [TestClass]
    public class GetQuizRequestValidatorTests : TestSetup
    {
        [TestMethod]
        public void ShouldBeValidGetQuiz()
        {
            var id = Guid.NewGuid();
            var quiz = new Quiz { Id = id, Name = "Quiz", Questions = new List<Question>() };
            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var getQuizRequest = new GetQuizRequest { Id = id };
            var validator = new GetQuizRequestValidator(repository);
            var result = validator.Validate(getQuizRequest);
            Assert.IsTrue(result.IsValid);
        }

        [TestMethod]
        public void ShouldBeInvalidGetQuizBadId()
        {
            var getQuizRequest = new GetQuizRequest { Id = Guid.NewGuid() };
            var repository = new FlinkDataRepository(new List<Quiz>(), new QuizValidator());
            var validator = new GetQuizRequestValidator(repository);
            var result = validator.Validate(getQuizRequest);
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Any(x => x.ErrorMessage == "Quiz cannot be found."));
        }
    }
}