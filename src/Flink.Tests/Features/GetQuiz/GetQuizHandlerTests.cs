﻿using System;
using System.Collections.Generic;
using System.Threading;
using Flink.DAL;
using Flink.Features.GetQuiz;
using Flink.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flink.Tests.Features.GetQuiz
{
    [TestClass]
    public class GetQuizHandlerTests : TestSetup
    {
        [TestMethod]
        public void ReturnCorrectTestWithQuestions()
        {
            var id = Guid.NewGuid();
            var quizName = "Quiz Name";
            var questions = new List<Question>()
            {
                new Question
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Question 1",
                    Type = QuestionType.Text,
                    Answers = new List<string>()
                    {
                        "Answer 1",
                    },
                    AnswerChoices = new List<Answer>(),
                    ImageBase64 = "",
                    ImageAltText = "",
                    Order = 0
                }
            };
            var quiz = new Quiz { Id = id, Name = quizName, Questions = questions };
            var repository = new FlinkDataRepository(new List<Quiz>{ quiz }, new QuizValidator());

            var getQuizRequest = new GetQuizRequest { Id = id };
            var handler = new GetQuizRequestHandler(repository);
            var result = handler.Handle(getQuizRequest, new CancellationToken()).Result;
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, quizName);
            Assert.AreEqual(result.Questions.Count, 1);
        }
 
        [TestMethod]
        public void ReturnCorrectTestWithNoQuestions()
        {
            var id = Guid.NewGuid();
            var quizName = "Quiz Name";
            var quiz = new Quiz { Id = id, Name = quizName, Questions = new List<Question>()};
            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var getQuizRequest = new GetQuizRequest { Id = id };
            var handler = new GetQuizRequestHandler(repository);
            var result = handler.Handle(getQuizRequest, new CancellationToken()).Result;
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, quizName);
            Assert.AreEqual(result.Questions.Count, 0);
        }
    }
}
