﻿using System;
using System.Collections.Generic;
using System.Threading;
using Flink.DAL;
using Flink.Features.GetQuizzes;
using Flink.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flink.Tests.Features.GetQuizzes
{
    [TestClass]
    public class GetQuizzesHandlerTests : TestSetup
    {
        [TestMethod]
        public void ReturnCorrectTestWithQuestions()
        {
            var id = Guid.NewGuid();
            var quizName = "Quiz Name";
            var questions = new List<Question>()
            {
                new Question
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "Question 1",
                    Type = QuestionType.Text,
                    Answers = new List<string>
                    {
                        "Answer"
                    },
                    AnswerChoices = new List<Answer>(),
                    ImageBase64 = "",
                    ImageAltText = "",
                    Order = 0
                }
            };

            var quiz = new Quiz { Id = id, Name = quizName, Questions = questions };
            var repository = new FlinkDataRepository(new List<Quiz> { quiz }, new QuizValidator());

            var getQuizzesRequest = new GetQuizzesRequest();
            var handler = new GetQuizzesRequestHandler(repository);
            var result = handler.Handle(getQuizzesRequest, new CancellationToken()).Result;
            Assert.AreEqual(result.Count, 1);
            Assert.IsTrue(result.TrueForAll(x => x.Id == id && x.Name == quizName && x.NumberOfQuestions == 1));
        }
    }
}
