﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Flink.Tests.Infrastructure
{
    [TestClass]
    public class AutoMapperValidationTests : TestSetup
    {
        [TestMethod]
        public void AutoMapperConfigShouldBeValid()
        {
            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
