# Introduction
This is a simple project to allow people to take quizzes.

# Getting Started
This repository requires VS2017 and runs on ASP.Net Core.

Before you run:
- To test AUTH you will need to replace the appsettings values with your own Google API information

To run:
- Clone repository locally
- Install NodeJS with NPM (nodejs.org)
- Run rebuild script (Flink\Tools\Scripts\rebuild.cmd)
- Testing: Run project, run unit tests, run the 'ng test.cmd' script

# Making Changes
- Create a branch for your code
- Make changes
- Ensure code builds and tests pass
- To build changes on the client, run the 'npm run-script build' command (see above)
- Push branch to remote
- Create PR

# CI/CD
- When changes are made to master VSTS triggers a build
- If the build is successful VSTS pushes the release to Azure

# Adding/Editing Quizzes
Quizzes are stored in the \Flink\Flink\Data directory (and can be added to any subdirectory), are in JSON format, and are postfixed with ".quiz.json".

# Access
- The API is public here: https://flinkapi.azurewebsites.net/
- Swagger documentation: https://flinkapi.azurewebsites.net/swagger/