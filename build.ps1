﻿properties {
    $targetFramework = "netcoreapp2.0"
    $product = "Flink"
    $birth_year = 2018
    $company = "Headspring"
    $configuration = 'Release'
    $build_date = (get-date).ToString("o")

    if ($env:build_number -ne $NULL) {
        $build = $env:build_number
        $versionSuffix = ""
    } else {
        $build = '0'
        $versionSuffix = "dev"
    }

    $versionPrefix = "1.0.$build"

    new-alias octo (find-build-dependency "octo.exe")
}

task default -depends update
task update  -depends DotNetCliVersion, Compile, UpdateDevDatabase, UpdateTestDatabase, Test
task rebuild -depends DotNetCliVersion, Compile, RebuildDevDatabase, RebuildTestDatabase, Test
task ci -depends DotNetCliVersion, Compile, Test, Package

##################
# Compile & Test #
##################

task DotnetCliVersion {
    dotnet --version
}

task ProjectProperties {
    regenerate-file "src\Directory.build.props" @"
<Project>
    <PropertyGroup>
        <Product>$product</Product>
        <VersionPrefix>$versionPrefix</VersionPrefix>
        <VersionSuffix>$versionSuffix</VersionSuffix>
        <Company>$company</Company>
        <Copyright>$(get-copyright)</Copyright>
    </PropertyGroup>
</Project>
"@
}

task Clean -depends ProjectProperties {
    delete-directory .\artifacts
    exec { dotnet clean src -c $configuration /nologo }
}

task Restore {
    exec { dotnet restore src }
}

task Compile -depends Clean, Restore {
    exec { dotnet build src -c $configuration /nologo }
}

task Test -depends Compile {
    
    pushd src/$product.Tests
    exec { dotnet test --configuration $configuration --no-build --results-directory TestResults }
    popd
}

#######################
# Database Management #
#######################

task RebuildDevDatabase -depends Compile {
    deploy-database "Rebuild" "DEV"
}

task RebuildTestDatabase -depends Compile {
    deploy-database "Rebuild" "TEST"
}

task UpdateDevDatabase -depends Compile {
    deploy-database "Update" "DEV"
}

task UpdateTestDatabase -depends Compile {
    deploy-database "Update" "TEST"
}

function deploy-database($action, $env) {

    $connection_string = "Server=.\;Database=$($product)_$($env);Trusted_Connection=True"

    $db_scripts_dir = "src\$product.DatabaseMigration\Scripts"
    $roundhouse_version_file = "src\$product.DatabaseMigration\bin\$configuration\$targetFramework\$product.DatabaseMigration.dll"

    $roundhouse_exe_path = find-build-dependency "rh.exe"
    $roundhouse_dir = [System.IO.Path]::GetDirectoryName($roundhouse_exe_path)
    $roundhouse_output_dir = "$roundhouse_dir\output"

    gci -rec $db_scripts_dir -filter *.sql | % {
        $relative_script_path = $_.FullName.Substring((resolve-path $db_scripts_dir).Path.Length)

        if ($relative_script_path.Length -gt 70) {
            Write-Host "The following script name is too long, which may cause exceptions to be logged during deployments:" -ForegroundColor Yellow
            Write-Host "     $relative_script_path" -ForegroundColor Yellow
            throw "Migration script name is too long."
        }
    }

    Write-Host "Executing Roundhouse"
    Write-Host "    Action: $action"
    Write-Host "    Environment: $env"
    Write-Host "    Connection String: $connection_string"

    if ($action -eq "Update") {
        exec { & $roundhouse_exe_path --connectionstring $connection_string `
                                      --commandtimeout 300 `
                                      --env $env `
                                      --output $roundhouse_output_dir `
                                      --sqlfilesdirectory $db_scripts_dir `
                                      --versionfile $roundhouse_version_file `
                                      --transaction `
                                      --silent }
    }

    if ($action -eq "Rebuild") {
        exec { & $roundhouse_exe_path --connectionstring $connection_string `
                                      --commandtimeout 300 `
                                      --env $env `
                                      --output $roundhouse_output_dir `
                                      --silent `
                                      --drop }

        exec { & $roundhouse_exe_path --connectionstring $connection_string `
                                      --commandtimeout 300 `
                                      --env $env `
                                      --output $roundhouse_output_dir `
                                      --sqlfilesdirectory $db_scripts_dir `
                                      --versionfile $roundhouse_version_file `
                                      --transaction `
                                      --silent `
                                      --simple }
    }
}

#############
# Packaging #
#############

task Package {
    # Prepare Database Migrations Packaging Folder
    create-directory .\artifacts\migration
    copy (find-build-dependency "rh.exe") .\artifacts\migration
    copy .\src\$product.DatabaseMigration\bin\$configuration\$targetFramework\$product.DatabaseMigration.dll .\artifacts\migration
    copy .\src\$product.DatabaseMigration\deploy.ps1 .\artifacts\migration
    copy .\src\$product.DatabaseMigration\Scripts\ .\artifacts\migration -recurse

    # Create Deployment Packages
    create-octopus-package "$product" .\src\$product\bin\$configuration\$targetFramework
    create-octopus-package "$product.DatabaseMigration" .\artifacts\migration
}

function create-octopus-package($id, $base_path, $format="zip") {
    $description = "$id deployment package, built on $build_date"

    exec { & octo pack --id $id `
                       --version $versionPrefix `
                       --basePath $base_path `
                       --author $company `
                       --format $format `
                       --outFolder .\artifacts\packages `
                       --description $description }
}

####################
# Helper Functions #
####################

function get-copyright {
    $date = Get-Date
    $year = $date.Year
    $copyright_span = if ($year -eq $birth_year) { $year } else { "$birth_year-$year" }
    return "Copyright © $copyright_span $company"
}

function regenerate-file($path, $new_content) {
    $oldContent = [IO.File]::ReadAllText($path)

    if ($new_content -ne $oldContent) {
        $relative_path = Resolve-Path -Relative $path
        write-host "Generating $relative_path"
        [System.IO.File]::WriteAllText($path, $new_content, [System.Text.Encoding]::UTF8)
    }
}

function find-build-dependency($exe_name) {
    $exes = @(gci src\Build-Dependencies\packages -rec -filter $exe_name)

    if ($exes.Length -ne 1)
    {
        throw "Expected to find 1 $exe_name, but found $($exes.Length)."
    }

    return $exes[0].FullName
}

function create-directory($path) {
    new-item -itemtype Directory -path $path | out-null
}

function delete-directory($path) {
    if (test-path $path) {
        write-host "Removing $path"
        rd $path -recurse -force -ErrorAction SilentlyContinue | out-null
    }
}